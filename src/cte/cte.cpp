#include "cte.hpp"

#include <stdexcept>
#include <forward_list>
#include <valarray>
#include <chrono>
#include <cmath>
#include <ostream>
#include <sstream>
#include <unistd.h>
#include <stdexcept>
#include <iomanip>
#include <fstream>

#include <Eigen/Dense>
#include <Eigen/SVD>

#include <fusion.h>

#include "libqhullcpp/Qhull.h"
#include "libqhullcpp/RboxPoints.h"
#include "libqhullcpp/QhullQh.h"

namespace ConvexTransformationEstimation {

template<typename point_type,
         typename points_type>
std::vector< std::vector<double> >
calculate_convex_hull_facets2(int dimension,
                              points_type & point_cloud)
{
  orgQhull::RboxPoints box_of_points;
  //orgQhull::Qconvex qconvex;
  orgQhull::Qhull qhull;

  //std::cout << point_cloud.size() << std::endl;
  
  std::stringstream qhull_input("");
  qhull_input << (dimension) << ' ';
  qhull_input << point_cloud.size();
  for(point_type & point : point_cloud)
  {
    for(int i=0;i<dimension;i++)
    {
      qhull_input << ' ' << point[i];
    }
  }
  
  std::istringstream is(qhull_input.str());
  box_of_points.appendPoints(is);
  qhull.runQhull(box_of_points, "");

  std::stringstream qhull_output;
  qhull.setOutputStream(&qhull_output);
  qhull.outputQhull("n");
  //  std::cout << qhull_output.str();

  int dim;
  int num_facets;
  qhull_output >> dim;
  qhull_output >> num_facets;
  //  std::cout << "Num facets: " << num_facets << std::endl;
  std::vector< std::vector<double> > facet_normals_and_offsets(num_facets);
  for(auto & f : facet_normals_and_offsets)
  {
    f.resize(dim);
    for(int i=0;i<dim;i++)
    {
      qhull_output >> f[i];
    }
  }

  return facet_normals_and_offsets;
}

template<typename point_type,
         typename points_type>
std::pair<std::vector< std::vector<double> >, std::vector< std::vector<double> > >
calculate_convex_hull_facets_and_vertices2(int dimension,
                                           points_type & point_cloud)
{
  orgQhull::RboxPoints box_of_points;
  //orgQhull::Qconvex qconvex;
  orgQhull::Qhull qhull;

  //std::cout << point_cloud.size() << std::endl;
  
  std::stringstream qhull_input("");
  qhull_input << (dimension) << ' ';
  qhull_input << point_cloud.size();
  for(point_type & point : point_cloud)
  {
    for(int i=0;i<dimension;i++)
    {
      qhull_input << ' ' << point[i];
    }
  }
  
  std::istringstream is(qhull_input.str());
  box_of_points.appendPoints(is);
  qhull.runQhull(box_of_points, "");

  std::stringstream qhull_output;
  qhull.setOutputStream(&qhull_output);
  qhull.outputQhull("n p");
  //  std::cout << qhull_output.str();

  int dim;
  int num_facets;
  qhull_output >> dim;
  qhull_output >> num_facets;
  //  std::cout << "Num facets: " << num_facets << std::endl;
  std::vector< std::vector<double> > facet_normals_and_offsets(num_facets);
  for(auto & f : facet_normals_and_offsets)
  {
    f.resize(dim);
    for(int i=0;i<dim;i++)
    {
      qhull_output >> f[i];
    }
  }


  int dim_vertices;
  int num_vertices;
  qhull_output >> dim_vertices;
  qhull_output >> num_vertices;
  std::vector< std::vector<double> > vertices(num_vertices);
  for(auto & f : vertices)
  {
    f.resize(dim_vertices);
    for(int i=0;i<dim_vertices;i++)
    {
      qhull_output >> f[i];
    }
  }
  
  auto facets_and_vertices = std::make_pair(facet_normals_and_offsets,
                                            vertices);
  
  return facets_and_vertices;
}

void
GraphAlignmentProblem2D::calculate_dissimilarity_values()
{
  for (int query_index=0;
       query_index < this->_num_query_points;
       query_index++)
  {
    this->_dissimilarity_values[query_index].resize(this->_num_reference_points);
    for(int reference_index=0;
        reference_index < this->_num_reference_points;
        reference_index++)
    {
      double dissimilarity = this->_dissimilarity_function(query_index, reference_index);
      this->_dissimilarity_values[query_index][reference_index] = dissimilarity;

      if(this->_min_dissimilarity > dissimilarity) this->_min_dissimilarity = dissimilarity;
      if(this->_max_dissimilarity < dissimilarity) this->_max_dissimilarity = dissimilarity;
      
      //this->_dissimilarity_values[query_index][reference_index] =
      //          this->_dissimilarity_function(query_index, reference_index);
    }
  }
  //std::cout << "Max Dissimilarity: " << this->_max_dissimilarity << ", " <<
  //      " Min: " << this->_min_dissimilarity << std::endl;
}

void
GraphAlignmentProblem2D::calculate_min_max_dimension_values()
{
  for(auto & p : this->_query_feature_point_locations)
  {
    if(p->x() < this->_min_dimension) this->_min_dimension = p->x();
    if(p->y() < this->_min_dimension) this->_min_dimension = p->y();
    if(p->x() > this->_max_dimension) this->_max_dimension = p->x();
    if(p->y() > this->_max_dimension) this->_max_dimension = p->y();        
  }
  for(auto & p : this->_reference_feature_point_locations)
  {
    if(p->x() < this->_min_dimension) this->_min_dimension = p->x();
    if(p->y() < this->_min_dimension) this->_min_dimension = p->y();
    if(p->x() > this->_max_dimension) this->_max_dimension = p->x();
    if(p->y() > this->_max_dimension) this->_max_dimension = p->y();        
  }

  //  std::cout << "Max Dimension: " << this->_max_dimension << ", " <<
  //      " Min: " << this->_min_dimension << std::endl;
}


Point2D get_projected_point_2D(const SolverResults& result,
                               const Point2D& point,
                               int j,
                               const GraphAlignmentProblem& problem)
{
  auto R = result.get_R();
  auto t = result.get_t();

  double x = problem.shift_and_scale_coord(point.x())*R[0] +
      problem.shift_and_scale_coord(point.y())*R[1] + t[0];
  double y = problem.shift_and_scale_coord(point.x())*R[2] +
      problem.shift_and_scale_coord(point.y())*R[3] + t[1];

  if(result.has_d())
  {
    x = x + result.get_d_jk(j, 0);
    y = y + result.get_d_jk(j, 1);
  }

  Point2D transformed_point(x, y);
  return transformed_point;
}

Point3D get_projected_point_3D(const SolverResults& result,
                               const Point3D& point,
                               int j,
                               const GraphAlignmentProblem& problem)                               
{
  
  auto R = result.get_R();
  auto t = result.get_t();

  double x = problem.shift_and_scale_coord(point.x())*R[0] + problem.shift_and_scale_coord(point.y())*R[1] + problem.shift_and_scale_coord(point.z())*R[2] + t[0];
  double y = problem.shift_and_scale_coord(point.x())*R[3] + problem.shift_and_scale_coord(point.y())*R[4] + problem.shift_and_scale_coord(point.z())*R[5] + t[1];
  double z = problem.shift_and_scale_coord(point.x())*R[6] + problem.shift_and_scale_coord(point.y())*R[7] + problem.shift_and_scale_coord(point.z())*R[8] + t[2];

  if(result.has_d())
  {
    x = x + result.get_d_jk(j, 0);
    y = y + result.get_d_jk(j, 1);
    z = z + result.get_d_jk(j, 2);
  }

  Point3D transformed_point(x, y, z);
  return transformed_point;
}

std::vector< std::vector<double> >
GraphAlignmentProblem2D::get_lower_convex_hull_plane_functions_for_xi_j(
    int j,
    int iteration_number,
    double trust_region_size,
    SolverResults prior_result) const
{
  // Create Point Cloud
  using value_type = double;
  using point = std::valarray< value_type >;
  using points = std::vector< point >;

  auto num_matched_points = this->_dissimilarity_values[j].size();

  //std::cout << "Finding Point Cloud...\n";
  points point_cloud;
  point_cloud.reserve(num_matched_points);

  if(iteration_number == 1)
  {
    for(int i=0; i< num_matched_points;i++)
    {
      auto dissimilarity = this->_dissimilarity_values[j][i];
      auto reference_point_location = this->_reference_feature_point_locations[i];

      double desired_max = 1.0;
      point p;

      p.resize(this->_dimension + 1);
      p[0] = this->shift_and_scale_coord(reference_point_location->x());
      p[1] = this->shift_and_scale_coord(reference_point_location->y());
      p[2] = dissimilarity * (desired_max/this->_max_dissimilarity);
      point_cloud.push_back(p);
      
      // point_cloud[i].resize(this->_dimension + 1);
      // point_cloud[i][0] = reference_point_location->x();
      // point_cloud[i][1] = reference_point_location->y();
      // //    point_cloud[i][2] = dissimilarity;
      // point_cloud[i][2] = dissimilarity * (desired_max/this->_max_dissimilarity);
      // //    std::cout << point_cloud[i][0] << ", " << point_cloud[i][1] << ", " << point_cloud[i][2] << "\n";
    }
  }
  else
  {
    auto query_point = this->_query_feature_point_locations[j];
    auto last_estimate_of_point_j = get_projected_point_2D(prior_result,
                                                           *query_point,
                                                           j,
                                                           *this);
    for(int i=0; i< num_matched_points;i++)
    {
      auto dissimilarity = this->_dissimilarity_values[j][i];
      auto reference_point_location = this->_reference_feature_point_locations[i];

      if( fabs(this->shift_and_scale_coord(reference_point_location->x()) -
               this->shift_and_scale_coord(last_estimate_of_point_j.x())) < (trust_region_size/2.0) &&
          fabs(this->shift_and_scale_coord(reference_point_location->y()) -
               this->shift_and_scale_coord(last_estimate_of_point_j.y())) < (trust_region_size/2.0) )
      {
        double desired_max = 1.0;
        point p;
        p.resize(this->_dimension + 1);
        p[0] = this->shift_and_scale_coord(reference_point_location->x());
        p[1] = this->shift_and_scale_coord(reference_point_location->y());
        p[2] = dissimilarity * (desired_max/this->_max_dissimilarity);
        point_cloud.push_back(p);
      }
      
      // double desired_max = 1.0;
      // point_cloud[i].resize(this->_dimension + 1);
      // point_cloud[i][0] = reference_point_location->x();
      // point_cloud[i][1] = reference_point_location->y();
      // //    point_cloud[i][2] = dissimilarity;
      // point_cloud[i][2] = dissimilarity * (desired_max/this->_max_dissimilarity);
      //    std::cout << point_cloud[i][0] << ", " << point_cloud[i][1] << ", " << point_cloud[i][2] << "\n";
    }

  }

  // Find convex hull
  //std::cout << "Finding Convex Hull...\n";

  if(point_cloud.size() < 5)
  {
    std::vector< std::vector<double> > empty_hull_function;
    return empty_hull_function;
  }
  
  auto facets_and_offsets = calculate_convex_hull_facets2<point, points>(this->_dimension+1,
                                                                         point_cloud);
  //   std::cout << "Returned num facets: " << facets_and_offsets.size() << "\n";
   // std::cout << "1st facet and offset: " << facets_and_offsets[0][0] << ", "
   //           << facets_and_offsets[0][1] << ", "
   //           << facets_and_offsets[0][2] << ", "
   //           << facets_and_offsets[0][3] << ", "
   //           << facets_and_offsets[0][4] << "\n";


  // Find plane functions for lower convex hull
  int m = 0;
  std::vector< std::vector<double> > plane_functions;
  for( auto & f : facets_and_offsets)
  {
    double distance = f[f.size()-1];
    double c_m = f[f.size()-2];
    double d_m = distance*(sqrt( std::pow(f[0], 2.0) +
                                 std::pow(f[1], 2.0) +
                                 std::pow(f[2], 2.0)));
    // std::cout << distance << std::endl;
    // std::cout << "Plane Equation : " << 
    //     f[0] << "*x + " <<
    //     f[1] << "*y + " <<
    //     f[2] << "*C + " <<
    //     d_m << " = 0\n";

    if(c_m < 0) // lower convex
    {
      std::vector< double > plane_function(this->_dimension + 1);
      plane_function[0] = - f[0]/c_m;
      plane_function[1] = - f[1]/c_m;
      plane_function[2] = - d_m/c_m;
      plane_functions.push_back(plane_function);
      // std::cout << "Plane Function m(" << m << "): C_m = " <<
      //     plane_function[0] << "*x + " <<
      //     plane_function[1] << "*y + " <<
      //     plane_function[2] << "\n";
      m++;
    }
  }

  return plane_functions;
}


const std::vector< std::vector< std::vector<double> > > 
GraphAlignmentProblem2D::get_lower_convex_hull_plane_functions(
    int iteration_number,
    double trust_region_size,
    SolverResults prior_result) const 
{
  //std::cout << "[ConvexTransformationEstimation] Started finding plane functions...\n";
  std::vector< std::vector< std::vector<double> > > plane_functions_for_j(
      this->num_query_points());;
  
  for(int j=0;j<this->num_query_points();j++)
  {
    plane_functions_for_j[j] = 
        this->get_lower_convex_hull_plane_functions_for_xi_j(j,
                                                             iteration_number,
                                                             trust_region_size,
                                                             prior_result);
  }

  //std::cout << "[ConvexTransformationEstimation] Finished finding plane functions.\n";
  return plane_functions_for_j;
}

void
GraphAlignmentProblem3D::calculate_dissimilarity_values()
{
  for (int query_index=0;
       query_index < this->_num_query_points;
       query_index++)
  {
    this->_dissimilarity_values[query_index].resize(this->_num_reference_points);
    for(int reference_index=0;
        reference_index < this->_num_reference_points;
        reference_index++)
    {
      double dissimilarity = this->_dissimilarity_function(query_index, reference_index);
      this->_dissimilarity_values[query_index][reference_index] = dissimilarity;

      if(this->_min_dissimilarity > dissimilarity) this->_min_dissimilarity = dissimilarity;
      if(this->_max_dissimilarity < dissimilarity) this->_max_dissimilarity = dissimilarity;

      //this->_dissimilarity_values[query_index][reference_index] =
      //          this->_dissimilarity_function(query_index, reference_index);
    }
  }
  //  std::cout << "Max Dissimilarity: " << this->_max_dissimilarity << ", " <<
  //      " Min: " << this->_min_dissimilarity << std::endl;
}

void
GraphAlignmentProblem3D::calculate_min_max_dimension_values()
{
  for(auto & p : this->_query_feature_point_locations)
  {
    if(p->x() < this->_min_dimension) this->_min_dimension = p->x();
    if(p->y() < this->_min_dimension) this->_min_dimension = p->y();
    if(p->z() < this->_min_dimension) this->_min_dimension = p->z();    
    if(p->x() > this->_max_dimension) this->_max_dimension = p->x();
    if(p->y() > this->_max_dimension) this->_max_dimension = p->y();
    if(p->z() > this->_max_dimension) this->_max_dimension = p->z();            
  }
  for(auto & p : this->_reference_feature_point_locations)
  {
    if(p->x() < this->_min_dimension) this->_min_dimension = p->x();
    if(p->y() < this->_min_dimension) this->_min_dimension = p->y();
    if(p->z() < this->_min_dimension) this->_min_dimension = p->z();        
    if(p->x() > this->_max_dimension) this->_max_dimension = p->x();
    if(p->y() > this->_max_dimension) this->_max_dimension = p->y();
    if(p->z() > this->_max_dimension) this->_max_dimension = p->z();                
  }

  //  std::cout << "Max Dimension: " << this->_max_dimension << ", " <<
  //      " Min: " << this->_min_dimension << std::endl;
}


std::vector< std::vector<double> >
GraphAlignmentProblem3D::get_lower_convex_hull_plane_functions_for_xi_j(
    int j,
    int iteration_number,
    double trust_region_size,
    SolverResults prior_result) const
{
  // Create Point Cloud
  using value_type = double;
  using point = std::valarray< value_type >;
  using points = std::vector< point >;

  auto num_matched_points = this->_dissimilarity_values[j].size();

  //  std::cout << "Finding Point Cloud...\n";
  points point_cloud;
  point_cloud.reserve(num_matched_points);

  double desired_max = 1.0;

  if(iteration_number == 1)
  {
    for(int i=0; i< num_matched_points;i++)
    {
      auto dissimilarity = this->_dissimilarity_values[j][i];
      auto reference_point_location = this->_reference_feature_point_locations[i];

      double desired_max = 1.0;
      point p;
      p.resize(this->_dimension + 1);
      p[0] = this->shift_and_scale_coord(reference_point_location->x());
      p[1] = this->shift_and_scale_coord(reference_point_location->y());
      p[2] = this->shift_and_scale_coord(reference_point_location->z());
      p[3] = dissimilarity * (desired_max/this->_max_dissimilarity);
      point_cloud.push_back(p);


      // point_cloud[i].resize(this->_dimension + 1);
      // point_cloud[i][0] = reference_point_location->x();
      // point_cloud[i][1] = reference_point_location->y();
      // point_cloud[i][2] = reference_point_location->z();
      // //    point_cloud[i][3] = dissimilarity;
      // point_cloud[i][3] = dissimilarity * (desired_max/this->_max_dissimilarity);
      // //    std::cout << point_cloud[i][0] << ", " << point_cloud[i][1] << ", " << point_cloud[i][2] << ", " <<
      // //        point_cloud[i][3] << "\n";
    }
  }
  else
  {
    auto query_point = this->_query_feature_point_locations[j];
    auto last_estimate_of_point_j = get_projected_point_3D(prior_result,
                                                           *query_point,
                                                           j,
                                                           *this);
    
    for(int i=0; i< num_matched_points;i++)
    {
      auto dissimilarity = this->_dissimilarity_values[j][i];
      auto reference_point_location = this->_reference_feature_point_locations[i];

      if( fabs(this->shift_and_scale_coord(reference_point_location->x()) -
               this->shift_and_scale_coord(last_estimate_of_point_j.x())) < (trust_region_size/2.0) &&
          fabs(this->shift_and_scale_coord(reference_point_location->y()) -
               this->shift_and_scale_coord(last_estimate_of_point_j.y())) < (trust_region_size/2.0) &&
          fabs(this->shift_and_scale_coord(reference_point_location->z()) -
               this->shift_and_scale_coord(last_estimate_of_point_j.z())) < (trust_region_size/2.0) )
      {

        double desired_max = 1.0;
        point p;
        p.resize(this->_dimension + 1);
        p[0] = this->shift_and_scale_coord(reference_point_location->x());
        p[1] = this->shift_and_scale_coord(reference_point_location->y());
        p[2] = this->shift_and_scale_coord(reference_point_location->z());
        p[3] = dissimilarity * (desired_max/this->_max_dissimilarity);
        point_cloud.push_back(p);
      }
      
      // point_cloud[i].resize(this->_dimension + 1);
      // point_cloud[i][0] = reference_point_location->x();
      // point_cloud[i][1] = reference_point_location->y();
      // point_cloud[i][2] = reference_point_location->z();
      // //    point_cloud[i][3] = dissimilarity;
      // point_cloud[i][3] = dissimilarity * (desired_max/this->_max_dissimilarity);
      // //    std::cout << point_cloud[i][0] << ", " << point_cloud[i][1] << ", " << point_cloud[i][2] << ", " <<
      // //        point_cloud[i][3] << "\n";
    }
  }
    
  
  // Find convex hull
  //  std::cout << "Finding Convex Hull...\n";
  if(point_cloud.size() < 5)
  {
    std::vector< std::vector<double> > empty_hull_function;
    return empty_hull_function;
  }

  auto facets_and_offsets = calculate_convex_hull_facets2<point, points>(this->_dimension+1,
                                                                         point_cloud);
   
  //  std::cout << "Returned num facets: " << facets_and_offsets.size() << "\n";
  // std::cout << "1st facet and offset: " << facets_and_offsets[0][0] << ", "
  //     << facets_and_offsets[0][1] << ", "
  //     << facets_and_offsets[0][2] << ", "
  //     << facets_and_offsets[0][3] << ", "
  //           << facets_and_offsets[0][4] << "\n";
  

  // value_type eps = 0;
  // using quick_hull_type = quick_hull< typename points::iterator >;
  // quick_hull_type quick_hull(this->_dimension+1, eps);

  // bool found_convex_hull =
  //     calculate_convex_hull_facets<quick_hull_type, points>(
  //         quick_hull, point_cloud);

  // Find plane functions for lower convex hull
  int m = 0;
  std::vector< std::vector<double> > plane_functions;
  for( auto & f : facets_and_offsets)
  {
    double distance = f[f.size()-1];
    double d_m = f[f.size()-2];
    double e_m = distance*(sqrt( std::pow(f[0], 2.0) +
                                 std::pow(f[1], 2.0) +
                                 std::pow(f[2], 2.0) +
                                 std::pow(f[3], 2.0) ));
    // std::cout << distance << std::endl;
    // std::cout << "Plane Equation : " << 
    //     f[0] << "*x + " <<
    //     f[1] << "*y + " <<
    //     f[2] << "*z + " <<
    //     f[3] << "*C + " <<
    //     e_m << " = 0\n";
    

    if(d_m < 0) // lower convex
    {
      std::vector< double > plane_function(this->_dimension + 1);
      plane_function[0] = - f[0]/d_m;
      plane_function[1] = - f[1]/d_m;
      plane_function[2] = - f[2]/d_m;
      plane_function[3] = - e_m/d_m;
      plane_functions.push_back(plane_function);
      // std::cout << "Plane Function m(" << m << "): C_m = " <<
      //     plane_function[0] << "*x + " <<
      //     plane_function[1] << "*y + " <<
      //     plane_function[2] << "*z + " <<
      //     plane_function[3] << "\n";
      m++;
    }
  }
  // for( auto & f : quick_hull.facets_)
  // {
  //     double distance = f.D;
  //     double d_m = f.normal_[this->_dimension];
  //     double e_m = distance*(sqrt( std::pow(f.normal_[0], 2.0) +
  //                                       std::pow(f.normal_[1], 2.0) +
  //                                       std::pow(f.normal_[2], 2.0) +
  //                                       std::pow(f.normal_[3], 2.0) ));
  //     std::cout << "Plane Equation : " << 
  //         f.normal_[0] << "*x + " <<
  //         f.normal_[1] << "*y + " <<
  //         f.normal_[2] << "*z + " <<
  //         f.normal_[3] << "*C + " <<
  //         e_m << " = 0\n";
    
  //   if (f.normal_[this->_dimension] < 0) // if in lower convex hull
  //   {
  //     std::vector< double > plane_function(this->_dimension + 1);
  //     // double distance = f.D;
  //     // double d_m = f.normal_[this->_dimension];
  //     // double e_m = distance*(sqrt( std::pow(f.normal_[0], 2.0) +
  //     //                                   std::pow(f.normal_[1], 2.0) +
  //     //                                   std::pow(f.normal_[2], 2.0) +
  //     //                                   std::pow(f.normal_[3], 2.0) ));
  //     // std::cout << "Plane Equation : " << 
  //     //     f.normal_[0] << "*x + " <<
  //     //     f.normal_[1] << "*y + " <<
  //     //     f.normal_[2] << "*z + " <<
  //     //     f.normal_[3] << "*C + " <<
  //     //     e_m = 0\n";
      
  //     plane_function[0] = - f.normal_[0]/d_m;
  //     plane_function[1] = - f.normal_[1]/d_m;
  //     plane_function[2] = - f.normal_[2]/d_m;
  //     plane_function[3] = - e_m/d_m;
  //     plane_functions.push_back(plane_function);
  //     std::cout << "Plane Function m(" << m << "): C_m = " <<
  //         plane_function[0] << "*x + " <<
  //         plane_function[1] << "*y + " <<
  //         plane_function[2] << "*z + " <<
  //         plane_function[3] << "\n";
  //     m++;
  //   }
  // }

  return plane_functions;
}


const std::vector< std::vector< std::vector<double> > > 
GraphAlignmentProblem3D::get_lower_convex_hull_plane_functions(
      int iteration_number,
      double trust_region_size,
      SolverResults prior_result) const 
{
  //  std::cout << "[ConvexTransformationEstimation] Started finding plane functions...\n";
  std::vector< std::vector< std::vector<double> > > plane_functions_for_j(
      this->num_query_points());;
  
  for(int j=0;j<this->num_query_points();j++)
  {
    plane_functions_for_j[j] = 
        this->get_lower_convex_hull_plane_functions_for_xi_j(j,
                                                             iteration_number,
                                                             trust_region_size,
                                                             prior_result);
  }

  //  std::cout << "[ConvexTransformationEstimation] Finished finding plane functions.\n";
  return plane_functions_for_j;
}

void
GraphAlignmentProblem3DKnownDepth::calculate_dissimilarity_values()
{
  for (int query_index=0;
       query_index < this->_num_query_points;
       query_index++)
  {
    this->_dissimilarity_values[query_index].resize(this->_num_reference_points);
    double query_point_depth =
        this->_query_feature_point_locations[query_index]->z();
    for(int reference_index=0;
        reference_index < this->_num_reference_points;
        reference_index++)
    {
      double reference_point_depth =
          this->_reference_feature_point_locations[reference_index]->z();

      if(fabs(query_point_depth - reference_point_depth) > this->_depth_difference_threshold)
      {
        this->_dissimilarity_values[query_index][reference_index] = -1;
      }
      else
      {
        double dissimilarity = this->_dissimilarity_function(query_index, reference_index);
        this->_dissimilarity_values[query_index][reference_index] = dissimilarity;

        if(this->_min_dissimilarity > dissimilarity) this->_min_dissimilarity = dissimilarity;
        if(this->_max_dissimilarity < dissimilarity) this->_max_dissimilarity = dissimilarity;
      }
    }
  }
  //  std::cout << "Max Dissimilarity: " << this->_max_dissimilarity << ", " <<
  //      " Min: " << this->_min_dissimilarity << std::endl;
}

void
GraphAlignmentProblem3DKnownDepth::calculate_min_max_dimension_values()
{
  for(auto & p : this->_query_feature_point_locations)
  {
    if(p->x() < this->_min_dimension) this->_min_dimension = p->x();
    if(p->y() < this->_min_dimension) this->_min_dimension = p->y();
    if(p->x() > this->_max_dimension) this->_max_dimension = p->x();
    if(p->y() > this->_max_dimension) this->_max_dimension = p->y();        
  }
  for(auto & p : this->_reference_feature_point_locations)
  {
    if(p->x() < this->_min_dimension) this->_min_dimension = p->x();
    if(p->y() < this->_min_dimension) this->_min_dimension = p->y();
    if(p->x() > this->_max_dimension) this->_max_dimension = p->x();
    if(p->y() > this->_max_dimension) this->_max_dimension = p->y();        
  }

  //  std::cout << "Max Dimension: " << this->_max_dimension << ", " <<
  //      " Min: " << this->_min_dimension << std::endl;
}

std::vector< std::vector<double> >
GraphAlignmentProblem3DKnownDepth::get_lower_convex_hull_plane_functions_for_xi_j(
    int j,
    int iteration_number,
    double trust_region_size,
    SolverResults prior_result) const
{
  // Create Point Cloud
  using value_type = double;
  using point = std::valarray< value_type >;
  using points = std::vector< point >;

  
  auto num_matched_points = this->_dissimilarity_values[j].size();

  //  std::cout << "Finding Point Cloud...\n";
  points point_cloud;
  point_cloud.reserve(num_matched_points);

  if(iteration_number == 1)
  {
    for(int i=0; i< num_matched_points;i++)
    {

      auto dissimilarity = this->_dissimilarity_values[j][i]; 
      auto reference_point_location = this->_reference_feature_point_locations[i];

      if(dissimilarity == -1)
      {
        continue;
      }
    
      point pnt;

      // Ignore z in convex hull calculation
      double desired_max = 1.0;
      pnt.resize(3);
      //      pnt[0] = reference_point_location->x();// * (desired_max/this->_max_dissimilarity);
      //      pnt[1] = reference_point_location->y();// * (desired_max/this->_max_dissimilarity);

      pnt[0] = this->shift_and_scale_coord(reference_point_location->x());
      pnt[1] = this->shift_and_scale_coord(reference_point_location->y());

      pnt[2] = dissimilarity * (desired_max/this->_max_dissimilarity);
      point_cloud.push_back(pnt);
      

    }
  }
  else
  {
    auto query_point = this->_query_feature_point_locations[j];
    auto last_estimate_of_point_j = get_projected_point_3D(prior_result,
                                                           *query_point,
                                                           j,
                                                           *this);
    
    for(int i=0; i< num_matched_points;i++)
    {

      auto dissimilarity = this->_dissimilarity_values[j][i]; 
      auto reference_point_location = this->_reference_feature_point_locations[i];

      if( fabs(this->shift_and_scale_coord(reference_point_location->x()) -
               this->shift_and_scale_coord(last_estimate_of_point_j.x())) < (trust_region_size/2.0) &&
          fabs(this->shift_and_scale_coord(reference_point_location->y()) -
               this->shift_and_scale_coord(last_estimate_of_point_j.y())) < (trust_region_size/2.0) )
    
      {

        if(dissimilarity == -1)
        {
          continue;
        }
    
        point pnt;

        // double desired_max = 1.0;
        // pnt.resize(this->_dimension + 1);
        // pnt[0] = reference_point_location->x();// * (desired_max/this->_max_dissimilarity);
        // pnt[1] = reference_point_location->y();// * (desired_max/this->_max_dissimilarity);
        // pnt[2] = reference_point_location->z();// * (desired_max/this->_max_dissimilarity);
        // pnt[3] = dissimilarity * (desired_max/this->_max_dissimilarity);
        // point_cloud.push_back(pnt);
        // //std::cout << pnt[0] << ", " << pnt[1] << ", " << pnt[2] << ", " << pnt[3] << "\n";

        // Ignore z in convex hull calculation
        double desired_max = 1.0;
        pnt.resize(3);
        //        pnt[0] = reference_point_location->x();// * (desired_max/this->_max_dissimilarity);
        //        pnt[1] = reference_point_location->y();// * (desired_max/this->_max_dissimilarity);

        pnt[0] = this->shift_and_scale_coord(reference_point_location->x());
        pnt[1] = this->shift_and_scale_coord(reference_point_location->y());
        pnt[2] = dissimilarity * (desired_max/this->_max_dissimilarity);
        point_cloud.push_back(pnt);
        //std::cout << pnt[0] << ", " << pnt[1] << ", " << pnt[2] << ", " << pnt[3] << "\n";
        
      }
    }
  }

  // Find convex hull
  //  std::cout << "Finding Convex Hull...\n";
  if(point_cloud.size() < 5)
  {
    std::vector< std::vector<double> > empty_hull_function;
    return empty_hull_function;
  }

  auto facets_and_offsets = calculate_convex_hull_facets2<point, points>(3,
                                                                         point_cloud);

  // Find plane functions for lower convex hull
  int m = 0;
  std::vector< std::vector<double> > plane_functions;
  for( auto & f : facets_and_offsets)
  {
    // double distance = f[f.size()-1];
    // double d_m = f[f.size()-2];
    // double e_m = distance*(sqrt( std::pow(f[0], 2.0) +
    //                              std::pow(f[1], 2.0) +
    //                              std::pow(f[2], 2.0) +
    //                              std::pow(f[3], 2.0) ));

    // if(d_m < 0) // lower convex
    // {
    //   std::vector< double > plane_function(this->_dimension + 1);
    //   plane_function[0] = - f[0]/d_m;
    //   plane_function[1] = - f[1]/d_m;
    //   plane_function[2] = - f[2]/d_m;
    //   plane_function[3] = - e_m/d_m;
    //   plane_functions.push_back(plane_function);
    //   m++;
    // }
    
    // Treat like 2d because we ignored z
    double distance = f[f.size()-1];
    double c_m = f[f.size()-2];
    double d_m = distance*(sqrt( std::pow(f[0], 2.0) +
                                 std::pow(f[1], 2.0) +
                                 std::pow(f[2], 2.0)));
    // std::cout << distance << std::endl;
    // std::cout << "Plane Equation : " << 
    //     f[0] << "*x + " <<
    //     f[1] << "*y + " <<
    //     f[2] << "*C + " <<
    //     d_m << " = 0\n";

    if(c_m < 0) // lower convex
    {
      std::vector< double > plane_function(3);
      plane_function[0] = - f[0]/c_m;
      plane_function[1] = - f[1]/c_m;
      plane_function[2] = - d_m/c_m;
      plane_functions.push_back(plane_function);
      // std::cout << "Plane Function m(" << m << "): C_m = " <<
      //     plane_function[0] << "*x + " <<
      //     plane_function[1] << "*y + " <<
      //     plane_function[2] << "\n";
      m++;
    }
    
  }

  return plane_functions;
}


const std::vector< std::vector< std::vector<double> > > 
GraphAlignmentProblem3DKnownDepth::get_lower_convex_hull_plane_functions(
      int iteration_number,
      double trust_region_size,
      SolverResults prior_result) const 
{
  //  std::cout << "[ConvexTransformationEstimation] Started finding plane functions...\n";
  std::vector< std::vector< std::vector<double> > > plane_functions_for_j(
      this->num_query_points());;
  
  for(int j=0;j<this->num_query_points();j++)
  {
    plane_functions_for_j[j] = 
        this->get_lower_convex_hull_plane_functions_for_xi_j(j,
                                                             iteration_number,
                                                             trust_region_size,
                                                             prior_result);
  }

  //  std::cout << "[ConvexTransformationEstimation] Finished finding plane functions.\n";
  return plane_functions_for_j;
}

const std::pair<std::vector<double>, std::vector<double> >
GraphAlignmentProblem2D::get_max_and_min_reference_dims() const
{
  double max_x = std::numeric_limits<double>::min();
  double max_y = std::numeric_limits<double>::min();
  double min_x = std::numeric_limits<double>::max();
  double min_y = std::numeric_limits<double>::max();
  for( auto p : this->_reference_feature_point_locations )
  {
    if(this->shift_and_scale_coord(p->x()) > max_x) max_x = this->shift_and_scale_coord(p->x());
    if(this->shift_and_scale_coord(p->y()) > max_y) max_y = this->shift_and_scale_coord(p->y());
    if(this->shift_and_scale_coord(p->x()) < min_x) min_x = this->shift_and_scale_coord(p->x());
    if(this->shift_and_scale_coord(p->y()) < min_y) min_y = this->shift_and_scale_coord(p->y());
  }
  auto max_and_min = 
      std::make_pair<std::vector<double>, std::vector<double> >({max_x, max_y}, {min_x, min_y});
  return max_and_min;
}

const std::pair<std::vector<double>, std::vector<double> >
GraphAlignmentProblem3D::get_max_and_min_reference_dims() const
{
  double max_x = std::numeric_limits<double>::min();
  double max_y = std::numeric_limits<double>::min();
  double max_z = std::numeric_limits<double>::min();
  double min_x = std::numeric_limits<double>::max();
  double min_y = std::numeric_limits<double>::max();
  double min_z = std::numeric_limits<double>::max();
  for( auto p : this->_reference_feature_point_locations )
  {
    if(this->shift_and_scale_coord(p->x()) > max_x) max_x = this->shift_and_scale_coord(p->x());
    if(this->shift_and_scale_coord(p->y()) > max_y) max_y = this->shift_and_scale_coord(p->y());
    if(this->shift_and_scale_coord(p->z()) > max_z) max_z = this->shift_and_scale_coord(p->z());
    if(this->shift_and_scale_coord(p->x()) < min_x) min_x = this->shift_and_scale_coord(p->x());
    if(this->shift_and_scale_coord(p->y()) < min_y) min_y = this->shift_and_scale_coord(p->y());
    if(this->shift_and_scale_coord(p->z()) < min_z) min_z = this->shift_and_scale_coord(p->z());
  }
  auto max_and_min = 
      std::make_pair<std::vector<double>, std::vector<double> >({max_x, max_y, max_z}, {min_x, min_y, max_z});
  return max_and_min;
}

const std::pair<std::vector<double>, std::vector<double> >
GraphAlignmentProblem3DKnownDepth::get_max_and_min_reference_dims() const
{
  double max_x = std::numeric_limits<double>::min();
  double max_y = std::numeric_limits<double>::min();
  double max_z = std::numeric_limits<double>::min();
  double min_x = std::numeric_limits<double>::max();
  double min_y = std::numeric_limits<double>::max();
  double min_z = std::numeric_limits<double>::max();
  for( auto p : this->_reference_feature_point_locations )
  {
    if(this->shift_and_scale_coord(p->x()) > max_x) max_x = this->shift_and_scale_coord(p->x());
    if(this->shift_and_scale_coord(p->y()) > max_y) max_y = this->shift_and_scale_coord(p->y());
    if(this->shift_and_scale_coord(p->z()) > max_z) max_z = this->shift_and_scale_coord(p->z());
    if(this->shift_and_scale_coord(p->x()) < min_x) min_x = this->shift_and_scale_coord(p->x());
    if(this->shift_and_scale_coord(p->y()) < min_y) min_y = this->shift_and_scale_coord(p->y());
    if(this->shift_and_scale_coord(p->z()) < min_z) min_z = this->shift_and_scale_coord(p->z());
  }
  auto max_and_min = 
      std::make_pair<std::vector<double>, std::vector<double> >({max_x, max_y, max_z}, {min_x, min_y, max_z});
  return max_and_min;
}



bool SolverResults::has_orthogonal_rotation_matrix() const
{
  auto R = this->get_R();
  
  double threshold = 0.1;
  if(this->get_Dim() == 2)
  {
    double dot_product00 = R[0]*R[0] + R[1]*R[1];
    double dot_product01 = R[0]*R[2] + R[1]*R[3];
    double dot_product10 = R[2]*R[0] + R[3]*R[1];
    double dot_product11 = R[2]*R[2] + R[3]*R[3];
    bool orthogonal = ( (fabs(dot_product00 - 1.0) <= threshold) &&
                        (fabs(dot_product01) <= threshold) &&
                        (fabs(dot_product10) <= threshold) &&
                        (fabs(dot_product11 - 1.0) <= threshold) );
    if (!orthogonal)
    {
      std::cout << "Not orthogonal, R * R^T: \n " <<
          dot_product00 << "\t" << dot_product01 << "\n" <<
          dot_product10 << "\t" << dot_product11 << "\n";
    }

    return orthogonal;
  }
  else if(this->get_Dim() == 3)
  {
    double dot_product00 = R[0]*R[0] + R[1]*R[1] + R[2]*R[2];
    double dot_product01 = R[0]*R[3] + R[1]*R[4] + R[2]*R[5];
    double dot_product02 = R[0]*R[6] + R[1]*R[7] + R[2]*R[8];
    double dot_product10 = R[3]*R[0] + R[4]*R[1] + R[5]*R[2];
    double dot_product11 = R[3]*R[3] + R[4]*R[4] + R[5]*R[5];
    double dot_product12 = R[3]*R[6] + R[4]*R[7] + R[5]*R[8];
    double dot_product20 = R[6]*R[0] + R[7]*R[1] + R[8]*R[2];
    double dot_product21 = R[6]*R[3] + R[7]*R[4] + R[8]*R[5];
    double dot_product22 = R[6]*R[6] + R[7]*R[7] + R[8]*R[8];    
    bool orthogonal = ( (fabs(dot_product00 - 1.0) <= threshold) &&
                        (fabs(dot_product01) <= threshold) &&
                        (fabs(dot_product02) <= threshold) &&
                        (fabs(dot_product10) <= threshold) &&
                        (fabs(dot_product11 - 1.0) <= threshold) &&
                        (fabs(dot_product12) <= threshold) &&
                        (fabs(dot_product20) <= threshold) &&
                        (fabs(dot_product21) <= threshold) &&
                        (fabs(dot_product22 - 1.0) <= threshold) );
    if (!orthogonal)
    {
      std::cout << "Not orthogonal, R * R^T: \n " <<
          dot_product00 << "\t" << dot_product01 << "\t" << dot_product02 << "\n" <<
          dot_product10 << "\t" << dot_product11 << "\t" << dot_product12 << "\n" <<
          dot_product20 << "\t" << dot_product21 << "\t" << dot_product22 << "\n";
    }

    return orthogonal;
  }
  else
  {
    std::cerr << "Invalid Dimension of result.\n";
    return false;
  }
}

bool SolverResults::has_correct_determinant() const
{
  auto R = this->get_R();
  
  double threshold = 0.1;
  if(this->get_Dim() == 2)
  {
    double determinant = R[0]*R[3] - R[1]*R[2];
    bool determinant_is_one = ( fabs(determinant - 1.0) <= threshold );
    if(!determinant_is_one)
    {
      std::cout << "Determinant is not one, determinant: " << determinant << std::endl;
      std::cout << "R:\n" << R[0] << ", " << R[1] << ";\n " << R[2] << ", " << R[3] << "\n";
    }

    return (determinant_is_one);
  }
  else if(this->get_Dim() == 3)
  {
    double determinant =
        R[0]*(R[4]*R[8] - R[5]*R[7]) -
        R[1]*(R[3]*R[8] - R[5]*R[6]) +
        R[2]*(R[3]*R[7] - R[4]*R[6]);
    bool determinant_is_one = ( fabs(determinant - 1.0) <= threshold );
    if(!determinant_is_one)
    {
      std::cout << "Determinant is not one, determinant: " << determinant << std::endl;
      std::cout << "R:\n" << R[0] << ", " << R[1] << ", " << R[2] << ";\n " <<
          R[3] << ", " << R[4] << ", " << R[5] << ";\n " <<
          R[6] << ", " << R[7] << ", " << R[8] << "\n";
    }

    return (determinant_is_one);
  }
  else
  {
    std::cerr << "Invalid Dimension of result.\n";
    return false;
  }
}

bool SolverResults::has_valid_rotation_matrix() const
{
  return (this->has_orthogonal_rotation_matrix() &&
          this->has_correct_determinant());
}

// Method implemented from
//
// https://mathoverflow.net/questions/86539/closest-3d-rotation-matrix-in-the-frobenius-norm-sense
//
// N. J. Higham. Matrix nearness problems and applications.
void
SolverResults::round_R()
{
  auto R_vec = this->get_R();
  auto dim = this->rotation_dim;

  Eigen::MatrixXd M(dim, dim);
  if(dim == 2)
  {
    M <<
        R_vec[0], R_vec[1],
        R_vec[2], R_vec[3];
        
  }
  else if(dim == 3)
  {
    M <<
        R_vec[0], R_vec[1], R_vec[2],
        R_vec[3], R_vec[4], R_vec[5],
        R_vec[6], R_vec[7], R_vec[8];        
  }
  else
  {
    std::cerr << "Invalid dimension of result.\n";
    return;
  }

  Eigen::JacobiSVD<Eigen::MatrixXd> svd(
      M,
      Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::MatrixXd R = svd.matrixU() * svd.matrixV();
  std::cout << "Determinant: " << R.determinant();
  if (fabs(R.determinant() - 1.0) > 0.01)
  {
    std::cout << " ... switching sign.";
    Eigen::MatrixXd U = svd.matrixU();
    U.col(dim-1) = U.col(dim-1)*-1;
    R = U * svd.matrixV();
  }
  std::cout << "\n";

  if( this->rotation_dim == this->dim )
  {
    std::vector<double> rounded_R_vec(dim*dim);
    for( int i=0;i<dim*dim;i++)
    {
      rounded_R_vec[i] = R(i);
    }

    this->R = rounded_R_vec;
  }
  else
  {
    // This handles the case where rotation is about xy, but z exists
    std::vector<double> rounded_R_vec(this->dim*this->dim);
    rounded_R_vec[0] = R(0);
    rounded_R_vec[1] = R(1);
    rounded_R_vec[2] = 0;
    rounded_R_vec[3] = R(2);
    rounded_R_vec[4] = R(3);
    rounded_R_vec[5] = 0;
    rounded_R_vec[6] = 0;
    rounded_R_vec[7] = 0;
    rounded_R_vec[8] = 1;

    this->R = rounded_R_vec;
  }
}

void SolverResults::print_unscaled_and_shifted_result(const SolverResults& result,
                                                      const GraphAlignmentProblem& problem)
{
  std::cout << "Dimension: " << result.dim << "\n";
  if(result.dim == 2)
  {
    std::cout << "[R_00, R_01,\n R_10, R_11]:\n[" <<
        result.R[0] << ", " << result.R[1] << ",\n " << 
        result.R[2] << ", " <<result.R[3] << "]\n";
  
    std::cout << "[t_0, t_1]:\n[" <<
        problem.undo_shift_and_scale_coord(result.t[0]) << ", " << problem.undo_shift_and_scale_coord(result.t[1]) << "]\n";
  }
  else
  {
    std::cout << "[R_00, R_01, R_02,\n R_10, R_11, R_12,\n R_20, R_21, R_22]:\n[" <<
        result.R[0] << ", " << result.R[1] << ", " << result.R[2] << ",\n " <<
        result.R[3] << ", " << result.R[4] << ", " << result.R[5] << ",\n " <<
        result.R[6] << ", " << result.R[7] << ", " << result.R[8] << "]\n";


    std::cout << "[t_0, t_1, t_2]:\n[" <<
        problem.undo_shift_and_scale_coord(result.t[0]) << ", " << problem.undo_shift_and_scale_coord(result.t[1]) <<
        ", " << problem.undo_shift_and_scale_coord(result.t[2]) << "]\n";
  }
}      

void SolverResults::unscale_and_shift_results(SolverResults& result,
                                              const GraphAlignmentProblem& problem)
{
  for(auto & val : result.t)
  {
    val = problem.undo_shift_and_scale_coord(val);
  }
  for(auto & d_temp : result.d)
  {
    for(auto & val : d_temp)
    {
      val = problem.undo_shift_and_scale_coord(val);
    }
  }    
}

void
Solver::test_mosek()
{
  auto A1 = monty::new_array_ptr<double,1>({ 3.0, 2.0, 0.0, 1.0 });
  auto A2 = monty::new_array_ptr<double,1>({ 2.0, 3.0, 1.0, 1.0 });
  auto A3 = monty::new_array_ptr<double,1>({ 0.0, 0.0, 3.0, 2.0 });
  auto A4 = monty::new_array_ptr<double,1>({ 0.0, 0.0, 1.0, 0.0 });
  auto c  = monty::new_array_ptr<double,1>({ 3.0, 5.0, 1.0, 1.0 });

  mosek::fusion::Model::t M = new mosek::fusion::Model("lo1");
  auto _M  = monty::finally([&]() {M->dispose(); });

  M->setLogHandler([=](const std::string & msg) { std::cout << msg << std::flush; } );

  mosek::fusion::Variable::t x = M->variable("x", 4, mosek::fusion::Domain::greaterThan(0.0));

  M->constraint("c1", mosek::fusion::Expr::dot(A1, x), mosek::fusion::Domain::equalsTo(30.0));
  M->constraint("c2", mosek::fusion::Expr::dot(A2, x), mosek::fusion::Domain::greaterThan(15.0));
  M->constraint("c3", mosek::fusion::Expr::dot(A3, x), mosek::fusion::Domain::lessThan(25.0));
  M->constraint("c4", mosek::fusion::Expr::dot(A4, x), mosek::fusion::Domain::lessThan(10.0));

  M->objective("obj", mosek::fusion::ObjectiveSense::Maximize, mosek::fusion::Expr::dot(c, x));

  M->solve();
  auto sol = x->level();

  std::cout << "[x0,x1,x2,x3] = [" << (*sol)[0] << "," << (*sol)[1] << "," << (*sol)[2] << " ]\n";
   
}

void
FeatureAlignmentModelCreator::create_variables_for_problem(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem,
    const std::string name_extension)
{
  // Create Variables
  //std::cout << "[ConvexTransformationEstimation] Creating Variables for Problem" << name_extension << "...\n";
  mosek::fusion::Variable::t R = model->variable(
      "R"+ name_extension,
      monty::new_array_ptr<int, 1>({
          problem.dimension(), problem.dimension()}),
      //    mosek::fusion::Domain::inRange(-100, 100) );
      mosek::fusion::Domain::unbounded() );
  //std::cout << R->toString() << "\n";

  mosek::fusion::Variable::t t = model->variable(
      "t" + name_extension,
      problem.dimension(),
      mosek::fusion::Domain::unbounded());

  mosek::fusion::Variable::t u = model->variable(
      "u" + name_extension,
      problem.num_query_points(),
      mosek::fusion::Domain::unbounded());

  if(problem.local_deviations_allowed())
  {
    auto max_values = problem.max_local_deviations();
    auto min_d = mosek::fusion::Matrix::dense(
        std::make_shared<monty::ndarray<double,2> >(
            monty::ndarray<double,2>(
                monty::shape(problem.num_query_points(),
                             problem.dimension()),
                std::function<double(const monty::shape_t<2> &)>(
                    [max_values](const monty::shape_t<2> & p) {return -max_values[p[1]];}))));
    auto max_d = mosek::fusion::Matrix::dense(
        std::make_shared<monty::ndarray<double,2> >(
            monty::ndarray<double,2>(
                monty::shape(problem.num_query_points(),
                             problem.dimension()),
                std::function<double(const monty::shape_t<2> &)>(
                    [max_values](const monty::shape_t<2> & p) {return max_values[p[1]];}))));    
    mosek::fusion::Variable::t d = model->variable(
        "d" + name_extension,
        monty::new_array_ptr<int, 1>({
            problem.num_query_points(), problem.dimension()}),
        mosek::fusion::Domain::inRange(min_d, max_d));
  }
}

mosek::fusion::Variable::t
FeatureAlignmentModelCreator::stack_variables_for_feature_alignment_constraint_matrix(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem,
    const std::string variable_extension)
{
  auto R = model->getVariable("R" + variable_extension);  
  auto t = model->getVariable("t" + variable_extension);
  auto u = model->getVariable("u" + variable_extension);

  //  std::shared_ptr<monty::ndarray<mosek::fusion::Variable::t, 1> >stacked_vars;
  if(problem.dimension() == 2 ||
     problem.transformation_restricted_to_xy_plane()) //
  {
    auto R_row0 = R->slice(monty::new_array_ptr<int, 1>({0,0}),
                           monty::new_array_ptr<int, 1>({1,2}))->transpose(); // R row 0
    auto t_0 = t->index(0);                                                  // t_0

    auto R_row1 = R->slice(monty::new_array_ptr<int, 1>({1,0}),
                           monty::new_array_ptr<int, 1>({2,2}))->transpose(); // R row 1
    auto t_1 = t->index(1);                                                  // t_1

    if(problem.local_deviations_allowed())
    {
      auto d = model->getVariable("d" + variable_extension);
      auto stacked_vars = monty::new_array_ptr<mosek::fusion::Variable::t,1>(
        {R_row0, t_0, R_row1, t_1, u, d});
      auto stacked_vector = mosek::fusion::Var::vstack(stacked_vars);
      return stacked_vector;            
    }    
    else
    {
      auto stacked_vars = monty::new_array_ptr<mosek::fusion::Variable::t,1>(
        {R_row0, t_0, R_row1, t_1, u});
      auto stacked_vector = mosek::fusion::Var::vstack(stacked_vars);
      return stacked_vector;            
    }
  }
  else
  {
    auto R_row0 = R->slice(monty::new_array_ptr<int, 1>({0,0}),
                           monty::new_array_ptr<int, 1>({1,3}))->transpose(); // R row 0
    auto t_0 = t->index(0);                                                  // t_0

    auto R_row1 = R->slice(monty::new_array_ptr<int, 1>({1,0}),
                           monty::new_array_ptr<int, 1>({2,3}))->transpose(); // R row 1
    auto t_1 = t->index(1);                                                  // t_1

    auto R_row2 = R->slice(monty::new_array_ptr<int, 1>({2,0}),
                           monty::new_array_ptr<int, 1>({3,3}))->transpose(); // R row 2
    auto t_2 = t->index(2);                                                  // t_2

    if(problem.local_deviations_allowed())
    {
      auto d = model->getVariable("d" + variable_extension);
      auto stacked_vars = monty::new_array_ptr<mosek::fusion::Variable::t,1>(
        {R_row0, t_0, R_row1, t_1, R_row2, t_2, u, d});
      auto stacked_vector = mosek::fusion::Var::vstack(stacked_vars);
      return stacked_vector;            
    }    
    else
    {
      auto stacked_vars = monty::new_array_ptr<mosek::fusion::Variable::t,1>(
        {R_row0, t_0, R_row1, t_1, R_row2, t_2, u});
      auto stacked_vector = mosek::fusion::Var::vstack(stacked_vars);
      return stacked_vector;      
    }
  }


}

FeatureAlignmentConstraintData
FeatureAlignmentModelCreator::construct_base_feature_alignment_constraint_matrix(
    const GraphAlignmentProblem & problem,
    int iteration_number,
    double trust_region_size,
    SolverResults prior_result,
    bool print_to_file)
{
  const std::vector< std::vector< std::vector<double> > >
      plane_functions_for_jth_feature_point = 
      problem.get_lower_convex_hull_plane_functions(iteration_number,
                                                    trust_region_size,
                                                    prior_result);
  const std::vector< std::shared_ptr<Point> >
      feature_point_locations = problem.query_feature_point_locations();


  if(print_to_file)
  {
    for(int j=0;j<problem.num_query_points();j++)
    {
      std::ofstream output;
      std::string name = "plane_functions/" + std::to_string(j) + ".csv";
      output.open(name);
      for(auto & pf : plane_functions_for_jth_feature_point[j])
      {
        output << pf[0];
        std::cout << pf[0];
        for(int i=1;i<pf.size();i++)
        {
          output << ", " << pf[i];
          std::cout << ", " << pf[i];
        }
        output << "\n";
        std::cout << "\n";
      }
      output.close();
      std::cout << "Saved file: " << name << std::endl;
    }

    auto dissimilarity_values = problem.dissimilarity_values();
    auto reference_points = problem.reference_feature_point_locations();
    
    for(int j=0;j<problem.num_query_points();j++)
    {
      std::ofstream output;
      std::string name = "dissimilarity/" + std::to_string(j) + ".csv";
      output.open(name);

      for(int i=0;i<problem.num_reference_points();i++)
      {
        auto dissimilarity = dissimilarity_values[j][i];
        if(dissimilarity != -1)
        {
          auto point = reference_points[i];
          output << point->x() << ", ";
          output << point->y() << ", ";
          if(problem.dimension() == 3)
          {
            output << std::static_pointer_cast<Point3D, Point>(point)->z() << ", ";
          }
          output << dissimilarity << "\n";
        }
      }
      output.close();
      std::cout << "Saved file: " << name << std::endl;
    }
  }
  

  //  std::cout << "[ConvexTransformationEstimation] Started creating feature constraints.\n";
  std::vector<int> sub_rows;
  std::vector<int> sub_cols;
  std::vector<double> sub_vals;

  std::vector<double> const_vals; // one per row

  std::vector<int> us_and_ds_to_zero;

  int current_row = 0;

  int num_ignored_points = 0;
  for (int j=0;j<problem.num_query_points();j++)
  {
    // If no plane function is returned for point j, fix associated vars and continue
    if(plane_functions_for_jth_feature_point[j].size()==0)
    {
      us_and_ds_to_zero.push_back(j);

      num_ignored_points++;
      continue;
    }

    int R_00_col, R_01_col, t_0_col; // Allways used, but needs
    int R_10_col, R_11_col, t_1_col; // Values reset if 3d
    int u_j_col, d_j_0_col, d_j_1_col;

    int R_02_col, R_12_col;          // These are only used in the
    int R_20_col, R_21_col, R_22_col, t_2_col; // 3D case
    int d_j_2_col;
        
    
    // int R_00_col = 0;  int R_01_col = 1; int t_0_col = 2; // Allways used, but needs
    // int R_10_col = 3;  int R_11_col = 4; int t_1_col = 5; // Values reset if 3d
    // int u_j_col = 6 + j; 
    // int d_j_0_col = 6 + problem.num_query_points() + j*2;
    // int d_j_1_col = 6 + problem.num_query_points() + j*2 + 1;

    // int R_02_col = 2; int R_12_col = 6;                                      // These are only used in the
    // int R_20_col = 8; int R_21_col = 9; int R_22_col = 10; int t_2_col = 11; // 3D case
    // int d_j_2_col = 12 + problem.num_query_points() + j*3 + 2;
    // Reinitializing here (shouldn't be initialized above really)
    if(problem.dimension() == 2 ||
       problem.transformation_restricted_to_xy_plane()) //
    {
      // Already correctly initialized
      R_00_col = 0;  R_01_col = 1; t_0_col = 2;
      R_10_col = 3;  R_11_col = 4; t_1_col = 5;
      u_j_col = 6 + j; 
      d_j_0_col = 6 + problem.num_query_points() + j*2;
      d_j_1_col = 6 + problem.num_query_points() + j*2 + 1;
    }
    else if(problem.dimension() == 3)
    {
      R_00_col = 0;  R_01_col = 1; R_02_col = 2; t_0_col = 3;
      R_10_col = 4;  R_11_col = 5; R_12_col = 6; t_1_col = 7;
      R_20_col = 8;  R_21_col = 9; R_22_col = 10; t_2_col = 11;      
      u_j_col = 12 + j; 
      d_j_0_col = 12 + problem.num_query_points() + j*3;
      d_j_1_col = 12 + problem.num_query_points() + j*3 + 1;
      d_j_2_col = 12 + problem.num_query_points() + j*3 + 2;      
    }
    else
    {
      std::cerr << "Invalid Dimension!\n";
      throw std::runtime_error ("Invalid Dimension");
    }
        
      
    double p_bj_x = problem.shift_and_scale_coord(feature_point_locations[j]->x());
    double p_bj_y = problem.shift_and_scale_coord(feature_point_locations[j]->y());
      
    for(auto & plane_function : plane_functions_for_jth_feature_point[j])
    {
      // R_00
      sub_rows.push_back(current_row); sub_cols.push_back(R_00_col);
      sub_vals.push_back(plane_function[0]*p_bj_x);

      // R_01
      sub_rows.push_back(current_row); sub_cols.push_back(R_01_col);
      sub_vals.push_back(plane_function[0]*p_bj_y);

      // t_0
      sub_rows.push_back(current_row); sub_cols.push_back(t_0_col);
      sub_vals.push_back(plane_function[0]);

      // R_10
      sub_rows.push_back(current_row); sub_cols.push_back(R_10_col);
      sub_vals.push_back(plane_function[1]*p_bj_x);

      // R_11
      sub_rows.push_back(current_row); sub_cols.push_back(R_11_col);
      sub_vals.push_back(plane_function[1]*p_bj_y);

      // t_1
      sub_rows.push_back(current_row); sub_cols.push_back(t_1_col);
      sub_vals.push_back(plane_function[1]);

      // u_j
      sub_rows.push_back(current_row); sub_cols.push_back(u_j_col);
      sub_vals.push_back(-1);        

      if(problem.dimension() == 2 ||
         problem.transformation_restricted_to_xy_plane()) //
      {
        // constant term
        //        std::cout <<"Adding Constant Term: " << plane_function[2] << "\n";
        const_vals.push_back(plane_function[2]);
      }
      else if(problem.dimension() == 3)// dimension == 3
      {
        double p_bj_z = problem.shift_and_scale_coord(
            std::static_pointer_cast<Point3D, Point>(feature_point_locations[j])->z());

        // R_02
        sub_rows.push_back(current_row); sub_cols.push_back(R_02_col);
        sub_vals.push_back(plane_function[0]*p_bj_z);

        // R_12
        sub_rows.push_back(current_row); sub_cols.push_back(R_12_col);
        sub_vals.push_back(plane_function[1]*p_bj_z);
         
        // R_20
        sub_rows.push_back(current_row); sub_cols.push_back(R_20_col);
        sub_vals.push_back(plane_function[2]*p_bj_x);

        // R_21
        sub_rows.push_back(current_row); sub_cols.push_back(R_21_col);
        sub_vals.push_back(plane_function[2]*p_bj_y);

        // R_21
        sub_rows.push_back(current_row); sub_cols.push_back(R_22_col);
        sub_vals.push_back(plane_function[2]*p_bj_z);        
 
        // t_2
        sub_rows.push_back(current_row); sub_cols.push_back(t_2_col);
        sub_vals.push_back(plane_function[2]);

        // constant term
        const_vals.push_back(plane_function[3]);
      }

      if(problem.local_deviations_allowed())
      {
        // d_j_0
        sub_rows.push_back(current_row); sub_cols.push_back(d_j_0_col);
        sub_vals.push_back(plane_function[0]);        

        // d_j_1
        sub_rows.push_back(current_row); sub_cols.push_back(d_j_1_col);
        sub_vals.push_back(plane_function[1]);

        if(problem.dimension() == 3 &&
           !problem.transformation_restricted_to_xy_plane())
        {
          // d_j_2
          sub_rows.push_back(current_row); sub_cols.push_back(d_j_2_col);
          sub_vals.push_back(plane_function[2]);
        }
      }
      current_row++;
    }
  }

  auto sub_i_array = std::make_shared<monty::ndarray<int,1> >(
      sub_rows.size(), sub_rows.begin(), sub_rows.end());
  auto sub_j_array = std::make_shared<monty::ndarray<int,1> >(
      sub_cols.size(), sub_cols.begin(), sub_cols.end());
  auto sub_vals_array = std::make_shared<monty::ndarray<double,1> >(
      sub_vals.size(), sub_vals.begin(), sub_vals.end());

  int num_columns = 6 + problem.num_query_points();
  if(problem.dimension() == 3 &&
     !problem.transformation_restricted_to_xy_plane())
  {
    num_columns += 6;
    if(problem.local_deviations_allowed())
    {
      num_columns += problem.num_query_points()*3;
    }
  }
  else if(problem.local_deviations_allowed())
  {
    num_columns += problem.num_query_points()*2;
  }
  
  auto constraints_matrix = mosek::fusion::Matrix::sparse(
      current_row,
      num_columns,
      sub_i_array, sub_j_array, sub_vals_array);


  std::vector<int> rows;
  for(int i=0;i<current_row;i++)
  {
    rows.push_back(i);
  }
  
  auto rows_array = std::make_shared<monty::ndarray<int,1> >(
      rows.size(), rows.begin(), rows.end());
  auto const_vals_array = std::make_shared<monty::ndarray<double,1> >(
      const_vals.size(), const_vals.begin(), const_vals.end());

  auto constants_matrix = mosek::fusion::Matrix::sparse(
      rows_array,
      std::make_shared<monty::ndarray<int,1> >(
          const_vals.size(), 0),
      const_vals_array);

  // std::function<double(const monty::shape_t<2> &)>(
  //             [const_vals](const monty::shape_t<2> & p) {return const_vals[p[0]];})));
  // //          const_vals.begin(), const_vals.end()));


  //  std::cout << "[ConvexTransformationEstimation] Finished creating feature constraints.\n";

  // std::cout << "Constraints matrix:\n";
  // for(int i=0;i<constraints_matrix->numRows();i++)
  // {
  //   for(int j=0;j<constraints_matrix->numColumns();j++)
  //   {
  //     std::cout << constraints_matrix->get(i, j) << ", ";
  //   }
  //   std::cout << "\n";
  // }
  // std::cout << "Constants matrix:\n";
  // for(int i=0;i<constants_matrix->numRows();i++)
  // {
  //   for(int j=0;j<constants_matrix->numColumns();j++)
  //   {
  //     std::cout << i << ", " << j << ", " << constants_matrix->get(i, j) << ", ";
  //   }
  //   std::cout << "\n";
  // }

  FeatureAlignmentConstraintData return_data(constraints_matrix,
                                             constants_matrix,
                                             us_and_ds_to_zero);
  return return_data;
}

void print_solution(mosek::fusion::Variable::t R,
                    mosek::fusion::Variable::t t)
{
  auto R_sol = R->level();
  auto t_sol = t->level();
  if(R_sol->size() == 4)
  {
    std::cout << "[R_00, R_01,\n R_10, R_11]:\n[" <<
        (*R_sol)[0] << ", " << (*R_sol)[1] << ",\n " << 
        (*R_sol)[2] << ", " <<(*R_sol)[3] << "]\n";
  
    std::cout << "[t_0, t_1]:\n[" <<
        (*t_sol)[0] << ", " << (*t_sol)[1] << "]\n";
  }
  else if(R_sol->size() == 9)
  {
    std::cout << R_sol->size() << "\n";
    std::cout << "[R_00, R_01, R_02,\n R_10, R_11, R_12,\n R_20, R_21, R_22]:\n[" <<
        (*R_sol)[0] << ", " << (*R_sol)[1] << ", " << (*R_sol)[2] << ",\n " <<
        (*R_sol)[3] << ", " << (*R_sol)[4] << ", " << (*R_sol)[5] << ",\n " <<
        (*R_sol)[6] << ", " << (*R_sol)[7] << ", " << (*R_sol)[8] << "]\n";


    std::cout << "[t_0, t_1, t_2]:\n[" <<
        (*t_sol)[0] << ", " << (*t_sol)[1] << ", " << (*t_sol)[2] << "]\n";
  }
  else
  {
    std::cerr <<  "Solution has invalid dimension: " << R_sol->size() << " \n.";
  }
}

void
Solver::specify_objective(mosek::fusion::Model::t model,
                          const GraphAlignmentProblem & problem)
{
  // Set Default Objective
  auto u = model->getVariable("u");
  auto obj = mosek::fusion::Expr::sum(u);
  model->objective("obj",
                   mosek::fusion::ObjectiveSense::Minimize,
                   obj);
}


SolverResults
Solver::solve_problem(const GraphAlignmentProblem & problem)
{
  SolverResults empty_result;
  auto result = this->solve_problem(problem, 1, std::numeric_limits<double>::max(), empty_result);
  SolverResults::unscale_and_shift_results(result, problem);
  return result;
}


void
FeatureAlignmentModelCreator::setup_base_feature_alignment_model(
    const GraphAlignmentProblem & problem,
    mosek::fusion::Model::t model,
    const FeatureAlignmentConstraintData & constraint_data,
    const std::string variable_extension)
{
  // Create Variables
  FeatureAlignmentModelCreator::create_variables_for_problem(model, problem, variable_extension);

  // Get data for minimizing transform parameters over feature distance
  auto stacked =
      FeatureAlignmentModelCreator::stack_variables_for_feature_alignment_constraint_matrix(
          model, problem, variable_extension);

  auto constraint_matrix_A = constraint_data.get_constraint_matrix();
  auto constant_matrix_b = constraint_data.get_constant_offset_matrix();
  
  // std::cout << constraint_matrix_A->numRows() << ", " << constraint_matrix_A->numColumns() << "\n";
  // std::cout << stacked->shape()->toString() << "\n";;
  // std::cout << constant_matrix_b->numRows() << ", " << constant_matrix_b->numColumns() << "\n";
  // // std::cout << constraint_matrix_A->toString();
  // // std::cout << constant_matrix_b->toString();

  
  // enforce constraints for minimizing feature distance
  auto constraint = mosek::fusion::Expr::add(
      mosek::fusion::Expr::mul(constraint_matrix_A, stacked),
      constant_matrix_b);
  model->constraint(constraint, mosek::fusion::Domain::lessThan(0.0));

  // constrain unused variables in the optimization problem
  auto j_vals_to_constrain = constraint_data.get_variables_to_constrain();
  auto u = model->getVariable("u" + variable_extension);
  for(auto j : j_vals_to_constrain)
  {
    model->constraint(u->index(j), mosek::fusion::Domain::equalsTo(0.0));
  }

  if(problem.local_deviations_allowed())
  {
    auto d = model->getVariable("d" + variable_extension);
    for(auto j : j_vals_to_constrain)
    {
      model->constraint(d->index(j,0), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(d->index(j,1), mosek::fusion::Domain::equalsTo(0.0));
      if(problem.dimension() == 3)
      {
        model->constraint(d->index(j,2), mosek::fusion::Domain::equalsTo(0.0));
      }
    }
  }
}

SolverResults
Solver::solve_problem(const GraphAlignmentProblem & problem,
                      int iteration_number,
                      double trust_region_size,
                      SolverResults prior_result)
{
  std::cout << "========================================================\nIteration: " <<
      iteration_number << "\n";
  std::cout << "Trust Region size: " << trust_region_size << "\n";
  std::cout << "Ignored Points On Last Iteration: " << prior_result.get_num_ignored_query_points() << "\n";

  
  // Create Model
  mosek::fusion::Model::t M = new mosek::fusion::Model("GraphAlignmentProblem");
  auto _M  = monty::finally([&]() { M->dispose(); });
  //  auto _M  = monty::finally([&]() { std::cout << "Deleting Model from solve problem.\n";M->dispose(); });  

  M->setLogHandler([=](const std::string & msg) { std::cout << msg << std::flush; } );

  // Create Variables and Constraints
  auto constraint_data = FeatureAlignmentModelCreator::construct_base_feature_alignment_constraint_matrix(
          problem,
          iteration_number,
          trust_region_size,
          prior_result);

  int num_ignored_features = constraint_data.get_variables_to_constrain().size();
  
  if(((num_ignored_features*1.0 - prior_result.get_num_ignored_query_points()) >
      0.10*prior_result.get_num_ignored_query_points()) &&
     (iteration_number > 1))
  {
    std::cout << "[ConvexTransformationEstimation] Trust region would cut off 10% of points from last iteration, stopping iterations.\n";
    prior_result.print();
    return prior_result;
  }

  if(num_ignored_features*1.0 > 0.40*problem.num_query_points() &&
     iteration_number > 1)
  {
    std::cout << "[ConvexTransformationEstimation] Trust region ignores 40% of query points, stopping iterations.\n";
    prior_result.print();
    return prior_result;
  }

  
  FeatureAlignmentModelCreator::setup_base_feature_alignment_model(problem,
                                                                   M,
                                                                   constraint_data);
  

  // Setup Objective
  this->specify_objective(M, problem);
  
  // Enforce Transformation Structure
  this->enforce_transformation_structure(M, problem);

  
  // Solve Problem
  this->solve_command(M, problem, constraint_data, num_ignored_features);

  
  //  std::cout << "Max Dissimilarity: " << problem.get_max_dissimilarity() << std::endl;

  // Either iterate or process and return results
  if(iteration_number >= this->max_iterations)
  {
    auto result = this->process_results(M, problem, num_ignored_features);
    M->dispose();
    return result;
  }
  else
  {
    auto result = this->process_results(M, problem, num_ignored_features);
      
    if(iteration_number == 1)
    {
      auto maxs_and_mins = problem.get_max_and_min_reference_dims();// TODO: determine size of trust region
      double max_distance = 0;
      for(int k=0;k<maxs_and_mins.first.size();k++)
      {
        double dist = maxs_and_mins.first[k] - maxs_and_mins.second[k];
        if (dist > max_distance) max_distance = dist;
      }

      return this->solve_problem(problem,
                                 ++iteration_number,
                                 max_distance*(2.0/3.0),
                                 result);
    }
    else
    {
      return this->solve_problem(problem,
                                 ++iteration_number,
                                 trust_region_size*(2.0/3.0),
                                 result);
    }
  }
}

std::vector<std::vector<double> > convert_d_to_vector(
    const GraphAlignmentProblem & problem,
    const mosek::fusion::Variable::t d_var)
{
  auto d_sol = d_var->level();
  std::vector< std::vector<double> > d;
  d.resize(problem.num_query_points());
  int j = 0;
  int k = 0;
  //  std::cout << "j:" << j << "\n";
  //  for( auto & val : (*d_sol) )
  for(int l=0;l<problem.num_query_points()*problem.dimension();l++)
  {
    auto val = (*d_sol)[l];
    if(k==0)
    {
      d[j].resize(problem.dimension());
    }
    d[j][k] = val;
    k++;
    if(k==problem.dimension())
    {
      k = 0;
      j++;
    }
  }
  return d;
}



SolverResults Solver::process_results(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem,
    int num_ignored_points)
{
  auto R = model->getVariable("R");  
  auto t = model->getVariable("t");
  
  auto R_sol = R->level();
  auto t_sol = t->level();

  if(problem.dimension() == 2)
  {
    print_solution(R, t);

    if(problem.local_deviations_allowed())
    {
      auto d = model->getVariable("d");      
      auto d_vec = convert_d_to_vector(problem, d);
      SolverResults result(2,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                           {(*t_sol)[0],(*t_sol)[1]},
                           num_ignored_points,
                           model->primalObjValue(),                           
                           d_vec);
      return result;
    }
    else
    {
      SolverResults result(2,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                           {(*t_sol)[0],(*t_sol)[1]},
                           num_ignored_points,
                           model->primalObjValue());
      return result;
    }
  }
  else if(problem.dimension() == 3)
  {
    print_solution(R, t);

    int rotation_dim;
    if(problem.transformation_restricted_to_xy_plane())
    {
      rotation_dim = 2;
    }
    else
    {
      rotation_dim = 3;
    }

    if(problem.local_deviations_allowed())
    {
      auto d = model->getVariable("d");      
      auto d_vec = convert_d_to_vector(problem, d);
      SolverResults result(3,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                 (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                 (*R_sol)[7],(*R_sol)[8]},
                           {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},
                           rotation_dim,
                           num_ignored_points,
                           model->primalObjValue(),                           
                           d_vec);
      return result;
    }
    else
    {
      SolverResults result(3,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                 (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                 (*R_sol)[7],(*R_sol)[8]},
                           {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},
                           rotation_dim,
                           num_ignored_points,
                           model->primalObjValue());
      return result;
    }
  }
}

void SolverAffine::enforce_transformation_structure(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem)
{
  auto R = model->getVariable("R");  
  auto t = model->getVariable("t");
  
  //  std::cout<< "[ConvexTransformationEstimation] Allowing Affine Transformations...\n";
  if (problem.dimension() == 2 ||
      problem.transformation_restricted_to_xy_plane())
  {
    if(problem.dimension() == 3)
    {
      //      std::cout<< "[ConvexTransformationEstimation] Constraining Rotation about z to identity...\n";
      model->constraint(R->index(0,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(1,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,0), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,1), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,2), mosek::fusion::Domain::equalsTo(1.0));

      if(problem.fix_z())
      {
        //        std::cout<< "[ConvexTransformationEstimation] Fixing z...\n";
        model->constraint(t->index(2), mosek::fusion::Domain::equalsTo(0.0));
      }
    }
  }
  else
  {
    std::cerr<< "Invalid or Unimplemented Dimensions\n" << std::endl;
    throw std::runtime_error ("Enforce Structure for 3-D Not implemented");    
  }
}

void SolverSymmetric::enforce_transformation_structure(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem)
{
  auto R = model->getVariable("R");  
  auto t = model->getVariable("t");
  
  //  std::cout<< "[ConvexTransformationEstimation] Restricting R to a Symmetric Transformation...\n";
  if (problem.dimension() == 2 ||
      problem.transformation_restricted_to_xy_plane())
  {
    auto C1 = model->constraint(mosek::fusion::Expr::sub(R->index(0,0),
                                                         R->index(1,1)),
                                mosek::fusion::Domain::equalsTo(0.0));
    //    std::cout << "Enforcing Constraint: " << C1->toString() << std::endl;
    auto C2 = model->constraint(mosek::fusion::Expr::add(R->index(0,1),
                                                         R->index(1,0)),
                                mosek::fusion::Domain::equalsTo(0.0));
    //    std::cout << "Enforcing Constraint: " << C2->toString() << std::endl;

    if(problem.dimension() == 3)
    {
      //      std::cout<< "[ConvexTransformationEstimation] Constraining Rotation about z to identity...\n";
      model->constraint(R->index(0,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(1,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,0), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,1), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,2), mosek::fusion::Domain::equalsTo(1.0));

      if(problem.fix_z())
      {
        //        std::cout<< "[ConvexTransformationEstimation] Fixing z...\n";
        model->constraint(t->index(2), mosek::fusion::Domain::equalsTo(0.0));
      }
    }

  }
  else
  {
    std::cerr<< "Invalid or Unimplemented Dimensions\n" << std::endl;
    throw std::runtime_error ("Enforce Structure for 3-D Not implemented");    
  }  
}


void SolverRigidBody_SOConvHull::enforce_transformation_structure(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem)
{
  auto R = model->getVariable("R");  
  auto t = model->getVariable("t");
  
  if (problem.dimension() == 2 ||
      problem.transformation_restricted_to_xy_plane())
  {
    //    std::cout<< "[ConvexTransformationEstimation] Restricting R to be Antisymmetric...\n";
    auto C1 = model->constraint(mosek::fusion::Expr::sub(R->index(0,0),
                                                         R->index(1,1)),
                                mosek::fusion::Domain::equalsTo(0.0));
    //    std::cout << "Enforcing Constraint: " << C1->toString() << std::endl;
    auto C2 = model->constraint(mosek::fusion::Expr::add(R->index(0,1),
                                                         R->index(1,0)),
                                mosek::fusion::Domain::equalsTo(0.0));

    if(problem.dimension() == 3)
    {
      //      std::cout<< "[ConvexTransformationEstimation] Constraining Rotation about z to identity...\n";
      model->constraint(R->index(0,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(1,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,0), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,1), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,2), mosek::fusion::Domain::equalsTo(1.0));

      if(problem.fix_z())
      {
        //        std::cout<< "[ConvexTransformationEstimation] Fixing z...\n";
        model->constraint(t->index(2), mosek::fusion::Domain::equalsTo(0.0));
      }
    }

    // if(this->clone_model_before_adding_psd_constraint)
    // {
    //   std::cout<< "[ConvexTransformationEstimation] Cloning model for later...\n";
    //   this->cloned_model = model->clone();
    // }
    
    //    std::cout<< "[ConvexTransformationEstimation] Constraining to ConvHull of SO(2)...\n";    
    auto cp1 = mosek::fusion::Expr::add(1.0, R->index(0,0));
    auto cm1 = mosek::fusion::Expr::sub(1.0, R->index(0,0));
    auto s = R->index(1,0);
    auto Eye2 = mosek::fusion::Matrix::eye(2);
    auto A2_row1 = mosek::fusion::Expr::hstack( cp1, s );
    auto A2_row2 = mosek::fusion::Expr::hstack( s, cm1 );
    auto A2 = mosek::fusion::Expr::vstack(A2_row1, A2_row2);
        //monty::new_array_ptr<mosek::fusion::Expression::t,2>(
        //      {{ cp1, s },
        //        { s, cm1 }});
    //    (*A2)[0][0] = cp1;
    //    (*A2)[0][1] = s;
    //    (*A2)[1][0] = s;
    //    (*A2)[1][1] = cm1;
    //        monty::shape(2,2)) =
    //    std::cout << "B\n";
    auto C3 = model->constraint(A2,
                                mosek::fusion::Domain::inPSDCone());

  }
  else
  {
    std::cerr<< "Invalid or Unimplemented Dimensions\n" << std::endl;
    throw std::runtime_error ("Enforce Structure for 3-D Not implemented");
  }
  
}

void round_rotation_if_needed(SolverResults& result)
{
  if(!result.has_valid_rotation_matrix())
  {
    std::cout << "Rotation Matrix is invalid!" << std::endl;
    result.round_R();
    auto rounded_r = result.get_R();
    std::cout << "Rounded R:\n";
    result.print();
    // std::cout << "[R_00, R_01,\n R_10, R_11]:\n[" <<
    //     rounded_r[0] << ", \t" << rounded_r[1] << ",\n " << 
    //     rounded_r[2] << ", \t" <<rounded_r[3] << "]\n";
    // std::cout << "R is now valid: " << result.has_valid_rotation_matrix() << "\n";
  }
  else
  {
    //    std::cout << "R is a valid rotation matrix." << std::endl;
  }
}

SolverResults SolverRigidBody_SOConvHull::process_results(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem,
    int num_ignored_points)
{
  auto R = model->getVariable("R");  
  auto t = model->getVariable("t");
  
  //  std::cout << "[ConvexTransformationEstimation] Processing SO(d)ConvHull Alignment Result..." << std::endl;
  auto R_sol = R->level();
  auto t_sol = t->level();

  if(problem.dimension() == 2)
  {
    print_solution(R, t);

    if(problem.local_deviations_allowed())
    {
      auto d = model->getVariable("d");      
      auto d_vec = convert_d_to_vector(problem, d);

      SolverResults result(2,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                           {(*t_sol)[0],(*t_sol)[1]},
                           num_ignored_points,
                           model->primalObjValue(),                           
                           d_vec);

      if(this->round_automatically)
      {
        round_rotation_if_needed(result);
      }
      return result;
    }
    else
    {
      SolverResults result(2,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                           {(*t_sol)[0],(*t_sol)[1]},
                           num_ignored_points,
                           model->primalObjValue());
      if(this->round_automatically)
      {
        round_rotation_if_needed(result);
      }
      return result;
    }
  }
  else if(problem.dimension() == 3)
  {
    print_solution(R, t);

    int rotation_dim;
    if(problem.transformation_restricted_to_xy_plane())
    {
      rotation_dim = 2;
    }
    else
    {
      rotation_dim = 3;
    }

    if(problem.local_deviations_allowed())
    {
      auto d = model->getVariable("d");      
      auto d_vec = convert_d_to_vector(problem, d);

      SolverResults result(3,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                 (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                 (*R_sol)[7],(*R_sol)[8]},
                           {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},
                           rotation_dim,
                           num_ignored_points,
                           model->primalObjValue(),                           
                           d_vec);
      if(this->round_automatically)
      {      
        round_rotation_if_needed(result);
      }
      return result;      
    }
    else
    {
      SolverResults result(3,
                           {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                 (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                 (*R_sol)[7],(*R_sol)[8]},
                           {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},
                           rotation_dim,
                           num_ignored_points,
                           model->primalObjValue());
      if(this->round_automatically)
      {            
        round_rotation_if_needed(result);
      }
      return result;
    }

  }
}

void L2NormApproximation2D::enforce_subproblem_constraints(mosek::fusion::Model::t model,
                                                           int section_number)
{
  bool use_extension = true;
  std::string extension = "";
  if(use_extension)
  {
    extension = "_" + std::to_string(section_number);
  }
  
  if(this->total_sections != 8)
  {
    std::cerr << "[ConvexTransformationEstimation] Not implemented 2D Approximation for num sections: " << this->total_sections << "\n";
    throw std::runtime_error ("L2NormApprox Not Implemented for Num Sections Called");
  }

  // We divide the sections by max magnitude and whether or not x1 and x2 are negative or positive
  // The order we follow is starting at 12:00 on the unit circle and following clockwise
  // The first column represents which element has max magnitude, the 2nd/3rd columns of x1/x2 respectivelly
  static const std::vector< std::vector<int> > divisions = { {2, 1, 1},
                                                             {1, 1, 1},
                                                             {1, 1, -1},
                                                             {2, 1, -1},
                                                             {2, -1, -1},
                                                             {1, -1, -1},
                                                             {1, -1, 1},
                                                             {2, -1, 1}};
  auto R = model->getVariable("R"+extension);  
  auto x1 = R->index(0, 0); //c
  auto x2 = R->index(1, 0); //s

  auto abs_x1 = mosek::fusion::Expr::mul( divisions[section_number][1], x1);
  auto abs_x2 = mosek::fusion::Expr::mul( divisions[section_number][2], x2);
  
  //  std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
  //      " - enforcing sub problem constraints\n";

  // Enforce max
  if(divisions[section_number][0] == 1)
  {
    auto C_max = model->constraint("max_x1_x2"+extension,
                                   mosek::fusion::Expr::sub(abs_x2, abs_x1),
                                   mosek::fusion::Domain::lessThan(0.0));
    //std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " constraint: " << C_max->toString() << "\n";            
  }
  else
  {
    auto C_max = model->constraint("max_x1_x2"+extension,
                                   mosek::fusion::Expr::sub(abs_x1, abs_x2),
                                   mosek::fusion::Domain::lessThan(0.0));
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " constraint: " << C_max->toString() << "\n";            
  }

  // Enforce Negative Postive
  if(divisions[section_number][1] == 1)
  {
    auto C_x1_sign = model->constraint("x1_sign"+extension, x1, mosek::fusion::Domain::greaterThan(0.0));
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " constraint: " << C_x1_sign->toString() << "\n";
  }
  else
  {
    auto C_x1_sign = model->constraint("x1_sign"+extension, x1, mosek::fusion::Domain::lessThan(0.0));
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " constraint: " << C_x1_sign->toString() << "\n";    
  }
  if(divisions[section_number][2] == 1)
  {
    auto C_x2_sign = model->constraint("x2_sign"+extension, x2, mosek::fusion::Domain::greaterThan(0.0));
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " constraint: " << C_x2_sign->toString() << "\n";    
  }
  else
  {
    auto C_x2_sign = model->constraint("x2_sign"+extension, x2, mosek::fusion::Domain::lessThan(0.0));
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " constraint: " << C_x2_sign->toString() << "\n";        
  }
  
  // auto C_x1_sign = model->constraint("x1_sign",
  //                                    mosek::fusion::Expr::mul(-1 * divisions[section_number][1], x1),
  //                                    mosek::fusion::Domain::lessThan(0.0));
  // auto C_x2_sign = model->constraint("x2_sign",
  //                                    mosek::fusion::Expr::mul(-1 * divisions[section_number][2], x2),
  //                                    mosek::fusion::Domain::lessThan(0.0));

  // Enforce Norm 1
  double alpha1 = 1;
  double alpha2 = sqrt(2) - 1;
  double delta = 2./(1. + sqrt( alpha1*alpha1 + alpha2*alpha2 ) );

  //  double epsilon_max = 0.04;
  double epsilon_max = 0.01;  

  //  mosek::fusion::Expr::t norm;
  if(divisions[section_number][0] == 1)
  {
    auto norm = mosek::fusion::Expr::add(
        mosek::fusion::Expr::mul(
            delta * alpha1,
            abs_x1),
        mosek::fusion::Expr::mul(
            delta * alpha2,
            abs_x2));
    
    // auto C_norm_1 = model->constraint("norm_approx_1",
    //                                   norm,
    //                                   mosek::fusion::Domain::inRange(1-epsilon_max, 1+epsilon_max));    
    auto C_norm_1 = model->constraint("norm_approx_1"+extension,
                                      norm,
                                      mosek::fusion::Domain::equalsTo(1.0));
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<    
    //        " constraint: " << C_norm_1->toString() << std::endl;
  }
  else
  {
    auto norm = mosek::fusion::Expr::add(
        mosek::fusion::Expr::mul(
            delta * alpha1,
            abs_x2),
        mosek::fusion::Expr::mul(
            delta * alpha2,
            abs_x1));

    // auto C_norm_1 = model->constraint("norm_approx_1",
    //                                   norm,
    //                                   mosek::fusion::Domain::inRange(1-epsilon_max, 1+epsilon_max));
    auto C_norm_1 = model->constraint("norm_approx_1"+extension,
                                      norm,
                                      mosek::fusion::Domain::equalsTo(1.0));
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<    
    //        " constraint: " << C_norm_1->toString() << std::endl;    
  }
}

void L2NormApproximation2D::solve_subproblem(mosek::fusion::Model::t model,
                                             int section_number)
{
  bool use_extension = true;
  std::string extension = "";
  if(use_extension)
  {
    extension = "_" + std::to_string(section_number);
  }
  
  // auto C_norm_minus_1 = model->constraint("norm-1",
  //                                         norm,
  //                                         //                                          mosek::fusion::Expr::sub(norm, 1),
  //                                         mosek::fusion::Domain::lessThan(epsilon_max + 1));
  // auto C_1_minus_norm = model->constraint("1-norm",
  //                                         mosek::fusion::Expr::mul(-1, norm),
  //                                         mosek::fusion::Domain::lessThan(epsilon_max - 1));
  
  std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
      " - solving problem...\n";
  try
  {
    model->solve();

    auto R = model->getVariable("R"+extension);  
    auto t = model->getVariable("t"+extension);
    auto R_sol = R->level();
    auto t_sol = t->level();

    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " - solution:\n";
    //    std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section_number <<
    //        " - solution value: " << model->primalObjValue()<< "\n";  
    if(this->problem.local_deviations_allowed())
    {
      auto d = model->getVariable("d"+extension);  
      auto d_vec = convert_d_to_vector(this->problem, d);

      if(this->problem.dimension() == 2)
      {
        SolverResults result(this->problem.dimension(),
                             {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                             {(*t_sol)[0],(*t_sol)[1]},
                             2, 
                             this->ignored_points,
                             model->primalObjValue(),
                             d_vec);
        this->set_thread_result(section_number, result);
        //        SolverResults::print_unscaled_and_shifted_result(result, problem);        
      }
      else
      {
        SolverResults result(this->problem.dimension(),
                             {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                   (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                   (*R_sol)[7],(*R_sol)[8]},
                             {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},      
                             2, 
                             this->ignored_points,
                             model->primalObjValue(),
                             d_vec);
        this->set_thread_result(section_number, result);
        //        SolverResults::print_unscaled_and_shifted_result(result, problem);        
      }
    }
    else
    {
      if(this->problem.dimension() == 2)
      {    
        SolverResults result(this->problem.dimension(),
                             {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                             {(*t_sol)[0],(*t_sol)[1]},
                             2, 
                             this->ignored_points,
                             model->primalObjValue());
        this->set_thread_result(section_number, result);
        //        SolverResults::print_unscaled_and_shifted_result(result, problem);
      }
      else
      {
        SolverResults result(this->problem.dimension(),
                             {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                   (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                   (*R_sol)[7],(*R_sol)[8]},
                             {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},                           
                             2, 
                             this->ignored_points,
                             model->primalObjValue());
        //        SolverResults::print_unscaled_and_shifted_result(result, problem);
        this->set_thread_result(section_number, result);
      }
    }
  }
  catch(mosek::fusion::SolutionError& e)
  {
    std::cout << e.toString() << std::endl;
  }
}

void L2NormApproximation2D::solve_subproblems(mosek::fusion::Model::t model,
                                              std::vector<int> section_numbers)
{
  // auto C_norm_minus_1 = model->constraint("norm-1",
  //                                         norm,
  //                                         //                                          mosek::fusion::Expr::sub(norm, 1),
  //                                         mosek::fusion::Domain::lessThan(epsilon_max + 1));
  // auto C_1_minus_norm = model->constraint("1-norm",
  //                                         mosek::fusion::Expr::mul(-1, norm),
  //                                         mosek::fusion::Domain::lessThan(epsilon_max - 1));

  std::string subproblems = "";
  for(auto problem : section_numbers)
  {
    subproblems += std::to_string(problem);
    subproblems += " ";
  }
  
  //Debug std::cout << "[ConvexTransformationEstimation] L2NormApproximation solving subproblems: " << subproblems << "\n";
  try
  {
    model->solve();

    for(auto section : section_numbers)
    {
      std::string extension = "_" + std::to_string(section);

      auto R = model->getVariable("R"+extension);  
      auto t = model->getVariable("t"+extension);
      auto R_sol = R->level();
      auto t_sol = t->level();

      auto u = model->getVariable("u"+extension);
      auto u_sol = *(u->level());
      double solution = 0;
      for(auto val : u_sol)
      {
        solution += val;
      }

      //      std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section <<
      //          " - solution:\n";
      //      std::cout << "[ConvexTransformationEstimation] L2NormApproximation Problem " << section <<
      //          " - solution value: " << solution << "\n";  
      if(this->problem.local_deviations_allowed())
      {
        auto d = model->getVariable("d"+extension);  
        auto d_vec = convert_d_to_vector(this->problem, d);

        if(this->problem.dimension() == 2)
        {
          SolverResults result(this->problem.dimension(),
                               {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                               {(*t_sol)[0],(*t_sol)[1]},
                               2, 
                               this->ignored_points,
                               solution,
                               d_vec);
          this->set_thread_result(section, result);
          //SolverResults::print_unscaled_and_shifted_result(result, problem);        
        }
        else
        {
          SolverResults result(this->problem.dimension(),
                               {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                     (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                     (*R_sol)[7],(*R_sol)[8]},
                               {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},      
                               2, 
                               this->ignored_points,
                               solution,
                               d_vec);
          this->set_thread_result(section, result);
          //SolverResults::print_unscaled_and_shifted_result(result, problem);        
        }
      }
      else
      {
        if(this->problem.dimension() == 2)
        {    
          SolverResults result(this->problem.dimension(),
                               {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3]},
                               {(*t_sol)[0],(*t_sol)[1]},
                               2, 
                               this->ignored_points,
                               solution);
          this->set_thread_result(section, result);
          //          SolverResults::print_unscaled_and_shifted_result(result, problem);
        }
        else
        {
          SolverResults result(this->problem.dimension(),
                               {(*R_sol)[0], (*R_sol)[1], (*R_sol)[2], (*R_sol)[3],
                                     (*R_sol)[4], (*R_sol)[5], (*R_sol)[6],
                                     (*R_sol)[7],(*R_sol)[8]},
                               {(*t_sol)[0],(*t_sol)[1],(*t_sol)[2]},                           
                               2, 
                               this->ignored_points,
                               solution);
          //          SolverResults::print_unscaled_and_shifted_result(result, problem);
          this->set_thread_result(section, result);
        }
      }
    }
  }
  catch(mosek::fusion::SolutionError& e)
  {
    std::cout << e.toString() << std::endl;
  }
}

SolverResults SolverRigidBody_L2LinearApprox::process_results(
    const GraphAlignmentProblem & problem,
    int num_ignored_points)
{
  //std::cout << "[ConvexTransformationEstimation] Processing L2LinearApprox Alignment Result..." << std::endl;

  auto result = this->best_result;

  if(this->round_automatically)
  {
    round_rotation_if_needed(result);
  }
  return result;
}

void SolverRigidBody_L2LinearApprox::enforce_transformation_structure(
    mosek::fusion::Model::t model,
    const GraphAlignmentProblem & problem,
    const std::string variable_extension)
{
  auto R = model->getVariable("R" + variable_extension);  
  auto t = model->getVariable("t" + variable_extension);
  
  if (problem.dimension() == 2 ||
      problem.transformation_restricted_to_xy_plane())
  {
    //    std::cout<< "[ConvexTransformationEstimation] Restricting R to be Antisymmetric...\n";
    auto C1 = model->constraint(mosek::fusion::Expr::sub(R->index(0,0),
                                                         R->index(1,1)),
                                mosek::fusion::Domain::equalsTo(0.0));
    //    std::cout << "Enforcing Constraint: " << C1->toString() << std::endl;
    auto C2 = model->constraint(mosek::fusion::Expr::add(R->index(0,1),
                                                         R->index(1,0)),
                                mosek::fusion::Domain::equalsTo(0.0));

    if(problem.dimension() == 3)
    {
      //      std::cout<< "[ConvexTransformationEstimation] Constraining Rotation about z to identity...\n";
      model->constraint(R->index(0,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(1,2), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,0), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,1), mosek::fusion::Domain::equalsTo(0.0));
      model->constraint(R->index(2,2), mosek::fusion::Domain::equalsTo(1.0));

      if(problem.fix_z())
      {
        //        std::cout<< "[ConvexTransformationEstimation] Fixing z...\n";
        model->constraint(t->index(2), mosek::fusion::Domain::equalsTo(0.0));
      }
    }

  }
  else
  {
    std::cerr<< "Invalid or Unimplemented Dimensions\n" << std::endl;
    throw std::runtime_error ("Enforce Structure for 3-D Not implemented");
  }
  
}


SolverResults
SolverRigidBody_L2LinearApprox::solve_problem(const GraphAlignmentProblem & problem)
{
  SolverResults empty_result;
  auto result = this->solve_problem(problem, 1, std::numeric_limits<double>::max(), empty_result);
  SolverResults::unscale_and_shift_results(result, problem);
  return result;
}

SolverResults
SolverRigidBody_L2LinearApprox::solve_problem(const GraphAlignmentProblem & problem,
                                    int iteration_number,
                                    double trust_region_size,
                                    SolverResults prior_result)
{
  std::cout << "========================================================\nIteration: " <<
      iteration_number << "\n";
  std::cout << "Trust Region size: " << trust_region_size << "\n";
  std::cout << "Ignored Points On Last Iteration: " << prior_result.get_num_ignored_query_points() << "\n";


  // Create Variables and Constraints
  auto constraint_data = FeatureAlignmentModelCreator::construct_base_feature_alignment_constraint_matrix(
          problem,
          iteration_number,
          trust_region_size,
          prior_result);

  int num_ignored_features = constraint_data.get_variables_to_constrain().size();
  
  if(((num_ignored_features*1.0 - prior_result.get_num_ignored_query_points()) >
      0.10*prior_result.get_num_ignored_query_points()) &&
     (iteration_number > 1))
  {
    std::cout << "Trust region would cut off 10% of points from last iteration, stopping iterations.\n";
    prior_result.print();
    return prior_result;
  }

  if(num_ignored_features*1.0 > 0.40*problem.num_query_points() &&
     iteration_number > 1)
  {
    std::cout << "Trust region ignores 40% of query points, stopping iterations.\n";
    prior_result.print();
    return prior_result;
  }

  // Solve the problem
  //std::cout << "[ConvexTransformationEstimation] Begin Solving Problem...\n";
  if(problem.dimension() == 2 ||
     problem.transformation_restricted_to_xy_plane())
  {
    // L2NormApproximation2D norm_approximation(model,
    //                                          problem,
    //                                          num_ignored_points);

    std::function<void(mosek::fusion::Model::t,
                       const GraphAlignmentProblem &,
                       const std::string)> enforce_function =
        [=] (mosek::fusion::Model::t model,
             const GraphAlignmentProblem & problem,
             const std::string variable_extension) {
      this->enforce_transformation_structure(model, problem, variable_extension);
    };
    
    L2NormApproximation2D norm_approximation(problem,
                                             constraint_data,
                                             enforce_function,
                                             num_ignored_features);    
    this->best_section = norm_approximation.run();
    this->best_result = norm_approximation.get_thread_result(best_section);
  }
  else
  {
    std::cerr << "L2 Norm Approximation not yet implemented for full 3D case\n";
    throw std::runtime_error ("Enforce Structure for 3-D Not implemented");          
  }

  //  std::cout << "[ConvexTransformationEstimation] Done Solving Problem...\n";
  
  //  std::cout << "Max Dissimilarity: " << problem.get_max_dissimilarity() << std::endl;

  // Either iterate or process and return results
  if(iteration_number >= this->max_iterations)
  {
    
    auto result = this->process_results(problem, num_ignored_features);
    return result;
  }
  else
  {
    auto result = this->process_results(problem, num_ignored_features);
      
    if(iteration_number == 1)
    {
      auto maxs_and_mins = problem.get_max_and_min_reference_dims();// TODO: determine size of trust region
      double max_distance = 0;
      for(int k=0;k<maxs_and_mins.first.size();k++)
      {
        double dist = maxs_and_mins.first[k] - maxs_and_mins.second[k];
        if (dist > max_distance) max_distance = dist;
      }

      return this->solve_problem(problem,
                                 ++iteration_number,
                                 max_distance*(2.0/3.0),
                                 result);
    }
    else
    {
      return this->solve_problem(problem,
                                 ++iteration_number,
                                 trust_region_size*(2.0/3.0),
                                 result);
    }
  }
}

} // namespace ConvexTransformationEstimation

