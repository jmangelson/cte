#include "cte/cte.hpp"

#include <iostream>
#include <numeric>
#include <memory>

#include "csv.h"

#include "gtest/gtest.h"


///////////////////////////////////////////////
// Auxiliary Functions
///////////////////////////////////////////////

std::shared_ptr<ConvexTransformationEstimation::Point2D>
get_transformed_point_2d(double x,
                         double y,
                         double theta=30,
                         double x_diff=20,
                         double y_diff=-10)
{
  double DEG_TO_RAD = 3.14159/180.;
  double c = cos(theta*DEG_TO_RAD);
  double s = sin(theta*DEG_TO_RAD);

  double new_x = x*c + y*s - (c*x_diff + s*y_diff);
  double new_y = x*(-s) + y*c - (c*y_diff - s*x_diff);

  return std::make_shared<ConvexTransformationEstimation::Point2D>(new_x, new_y);
}

std::shared_ptr<ConvexTransformationEstimation::Point3D>
get_transformed_point_zero_z(double x,
                      double y,
                      double z,
                      double theta=30,
                      double x_diff=20,
                      double y_diff=-10)
{
  double DEG_TO_RAD = 3.14159/180.;
  double c = cos(theta*DEG_TO_RAD);
  double s = sin(theta*DEG_TO_RAD);

  double new_x = x*c + y*s - (c*x_diff + s*y_diff);
  double new_y = x*(-s) + y*c - (c*y_diff - s*x_diff);
  double new_z = 0;

  return std::make_shared<ConvexTransformationEstimation::Point3D>(new_x, new_y, new_z);
}


double l2_dist(int feature_dim,
               const std::vector< std::vector<double> >& map_features,
               const std::vector< std::vector<double> >& traj_features,
               int i, // local map
               int j) // remote traj
{
  double distance = 0;
  double power = 2.0;
  for (int k=0;k<feature_dim;k++)
  {
    distance += pow( fabs(map_features[i][k] - traj_features[j][k]), power );
  }

  double normalized = distance / feature_dim;
  return normalized;
}

ConvexTransformationEstimation::SolverResults
Solve3DKnownDepthProblemFromFile(std::string reference_points_file,
                                 std::string query_points_file,
                                 std::string params_file,
                                 ConvexTransformationEstimation::Solver& solver)
{
  // Read test parameters
  io::CSVReader<7> in(params_file);
  in.read_header(io::ignore_no_column, "feature_dims", "depth_thresh", "fixed_z", "enable_local_deviations",
                 "max_deviations_x", "max_deviations_y", "max_deviations_z");
  int feature_dim;
  double depth_thresh;
  std::string fixed_z;
  std::string enable_local_deviations;
  double max_x, max_y, max_z;
  in.read_row(feature_dim, depth_thresh, fixed_z, enable_local_deviations, max_x, max_y, max_z);
  feature_dim = 3;

  // Get reference_points points/features
  std::vector<std::vector<double> > reference_features;
  typedef ConvexTransformationEstimation::Point3D Point_T;
  std::vector< std::shared_ptr<Point_T> > reference_points;

  io::CSVReader<6> in_reference_points(reference_points_file);
  //io::CSVReader<4> in_reference_points(reference_points_file);
  in_reference_points.read_header(io::ignore_no_column, "x", "y", "z", "f0", "f1", "f2");
  //in_reference_points.read_header(io::ignore_no_column, "x", "y", "z", "f0");
  double x, y, z, f0, f1, f2;
  while(in_reference_points.read_row(x, y, z, f0, f1, f2))
    //while(in_reference_points.read_row(x, y, z, f0))
  {
    //    f1 = 0;
    //    f2 = 0;
    auto point = std::make_shared<Point_T>(x, y, z);
    std::vector<double> feature = {f0, f1, f2};
    reference_features.push_back(feature);
    reference_points.push_back(point);
  }

  // Get trajectory points/features
  std::vector<std::vector<double> > query_features;
  typedef ConvexTransformationEstimation::Point3D Point_T;
  std::vector< std::shared_ptr<Point_T> > query_points;
  
  io::CSVReader<6> in_query_points(query_points_file);
  //io::CSVReader<4> in_query_points(query_points_file);
  in_query_points.read_header(io::ignore_no_column, "x", "y", "z", "f0", "f1", "f2");
  //in_reference_points.read_header(io::ignore_no_column, "x", "y", "z", "f0");  

  while(in_query_points.read_row(x, y, z, f0, f1, f2))
    //while(in_query_points.read_row(x, y, z, f0))
  {
    //    f1 = 0;
    //    f2 = 0;
    auto point = std::make_shared<Point_T>(x, y, z);
    std::vector<double> feature = {f0, f1, f2};
    query_features.push_back(feature);
    query_points.push_back(point);
  }

  // Create and solve problem
  std::function<double(int, int)> dissimilarity_function = [=](int j, int i) {
    return l2_dist(feature_dim,
                   reference_features,
                   query_features,
                   i,
                   j);
  };

  ConvexTransformationEstimation::GraphAlignmentProblem3DKnownDepth problem(
    query_points,
    reference_points,
    dissimilarity_function,
    depth_thresh,
    fixed_z.compare("True") == 0,
    enable_local_deviations.compare("True")==0,
    {max_x, max_y, max_z});

  return solver.solve_problem(problem);
}


ConvexTransformationEstimation::SolverResults
Solve2DProblemFromFile(std::string reference_points_file,
                       std::string query_points_file,
                       std::string params_file,
                       ConvexTransformationEstimation::Solver& solver)
{
  // Read test parameters
  io::CSVReader<7> in(params_file);
  in.read_header(io::ignore_no_column, "feature_dims", "depth_thresh", "fixed_z", "enable_local_deviations",
                 "max_deviations_x", "max_deviations_y", "max_deviations_z");
  int feature_dim;
  double depth_thresh;
  std::string fixed_z;
  std::string enable_local_deviations;
  double max_x, max_y, max_z;
  in.read_row(feature_dim, depth_thresh, fixed_z, enable_local_deviations, max_x, max_y, max_z);
  feature_dim = 3;

  // Get reference_points points/features
  std::vector<std::vector<double> > reference_features;
  typedef ConvexTransformationEstimation::Point2D Point_T;
  std::vector< std::shared_ptr<Point_T> > reference_points;

  io::CSVReader<6> in_reference_points(reference_points_file);
  //io::CSVReader<4> in_reference_points(reference_points_file);
  in_reference_points.read_header(io::ignore_no_column, "x", "y", "z", "f0", "f1", "f2");
  //in_reference_points.read_header(io::ignore_no_column, "x", "y", "z", "f0");
  double x, y, z, f0, f1, f2;
  while(in_reference_points.read_row(x, y, z, f0, f1, f2))
    //while(in_reference_points.read_row(x, y, z, f0))
  {
    //    f1 = 0;
    //    f2 = 0;
    auto point = std::make_shared<Point_T>(x, y);
    std::vector<double> feature = {f0, f1, f2};
    reference_features.push_back(feature);
    reference_points.push_back(point);
  }

  // Get trajectory points/features
  std::vector<std::vector<double> > query_features;
  typedef ConvexTransformationEstimation::Point2D Point_T;
  std::vector< std::shared_ptr<Point_T> > query_points;
  
  io::CSVReader<6> in_query_points(query_points_file);
  //io::CSVReader<4> in_query_points(query_points_file);
  in_query_points.read_header(io::ignore_no_column, "x", "y", "z", "f0", "f1", "f2");
  //in_reference_points.read_header(io::ignore_no_column, "x", "y", "z", "f0");  

  while(in_query_points.read_row(x, y, z, f0, f1, f2))
    //while(in_query_points.read_row(x, y, z, f0))
  {
    //    f1 = 0;
    //    f2 = 0;
    auto point = std::make_shared<Point_T>(x, y);
    std::vector<double> feature = {f0, f1, f2};
    query_features.push_back(feature);
    query_points.push_back(point);
  }

  // Create and solve problem
  std::function<double(int, int)> dissimilarity_function = [=](int j, int i) {
    return l2_dist(feature_dim,
                   reference_features,
                   query_features,
                   i,
                   j);
  };

  ConvexTransformationEstimation::GraphAlignmentProblem2D problem(
    query_points,
    reference_points,
    dissimilarity_function,
    enable_local_deviations.compare("True")==0,
    {max_x, max_y});

  return solver.solve_problem(problem);
}

////////////////////////////////////////////////////
// 2D Case Tests
///////////////////////////////////////////////////

TEST(Test2DProblemClass, RigidBodyTest1) {

  std::mt19937_64 rng(0);
  std::uniform_real_distribution<double> unif(-0.015,0.015);

  typedef ConvexTransformationEstimation::Point2D Point_T;
  std::vector< std::shared_ptr<Point_T> > points(35);
  int i = 0; double x = -2;
  points[i+0] = std::make_shared<Point_T>(x + unif(rng), 0.1 + unif(rng));
  points[i+1] = std::make_shared<Point_T>(x + unif(rng), 0 + unif(rng));
  points[i+2] = std::make_shared<Point_T>(x + unif(rng), 0.5 + unif(rng));
  points[i+3] = std::make_shared<Point_T>(x + unif(rng), 1.5 + unif(rng));
  points[i+4] = std::make_shared<Point_T>(x + unif(rng), 2.5 + unif(rng));
  points[i+5] = std::make_shared<Point_T>(x + unif(rng), 4 + unif(rng));
  points[i+6] = std::make_shared<Point_T>(x + unif(rng), 5 + unif(rng));
  i = 7; x = -1;
  points[i+0] = std::make_shared<Point_T>(x + unif(rng), 0.15 + unif(rng));
  points[i+1] = std::make_shared<Point_T>(x + unif(rng), 0 + unif(rng));
  points[i+2] = std::make_shared<Point_T>(x + unif(rng), 0.5 + unif(rng));
  points[i+3] = std::make_shared<Point_T>(x + unif(rng), 1.5 + unif(rng));
  points[i+4] = std::make_shared<Point_T>(x + unif(rng), 2.5 + unif(rng));
  points[i+5] = std::make_shared<Point_T>(x + unif(rng), 4 + unif(rng));
  points[i+6] = std::make_shared<Point_T>(x + unif(rng), 5 + unif(rng));
  i = 14; x = 0;
  points[i+0] = std::make_shared<Point_T>(x + unif(rng), 0.2 + unif(rng));
  points[i+1] = std::make_shared<Point_T>(x + unif(rng), 0 + unif(rng));
  points[i+2] = std::make_shared<Point_T>(x + unif(rng), 0.5 + unif(rng));
  points[i+3] = std::make_shared<Point_T>(x + unif(rng), 1.5 + unif(rng));
  points[i+4] = std::make_shared<Point_T>(x + unif(rng), 2.5 + unif(rng));
  points[i+5] = std::make_shared<Point_T>(x + unif(rng), 4 + unif(rng));
  points[i+6] = std::make_shared<Point_T>(x + unif(rng), 5 + unif(rng));
  i = 21; x = 1;
  points[i+0] = std::make_shared<Point_T>(x + unif(rng), 0.15 + unif(rng));
  points[i+1] = std::make_shared<Point_T>(x + unif(rng), 0 + unif(rng));
  points[i+2] = std::make_shared<Point_T>(x + unif(rng), 0.5 + unif(rng));
  points[i+3] = std::make_shared<Point_T>(x + unif(rng), 1.5 + unif(rng));
  points[i+4] = std::make_shared<Point_T>(x + unif(rng), 2.5 + unif(rng));
  points[i+5] = std::make_shared<Point_T>(x + unif(rng), 4 + unif(rng));
  points[i+6] = std::make_shared<Point_T>(x + unif(rng), 5 + unif(rng));
  i = 28; x = 2;
  points[i+0] = std::make_shared<Point_T>(x + unif(rng), 0.1 + unif(rng));
  points[i+1] = std::make_shared<Point_T>(x + unif(rng), 0 + unif(rng));
  points[i+2] = std::make_shared<Point_T>(x + unif(rng), 0.5 + unif(rng));
  points[i+3] = std::make_shared<Point_T>(x + unif(rng), 1.5 + unif(rng));
  points[i+4] = std::make_shared<Point_T>(x + unif(rng), 2.5 + unif(rng));
  points[i+5] = std::make_shared<Point_T>(x + unif(rng), 4 + unif(rng));
  points[i+6] = std::make_shared<Point_T>(x + unif(rng), 5 + unif(rng));

  std::vector< std::shared_ptr<Point_T> > points2(21);
  i = 0; x = -1;
  points2[i+0] = get_transformed_point_2d(x + unif(rng), 0.15 + unif(rng));
  points2[i+1] = get_transformed_point_2d(x + unif(rng), 0 + unif(rng));
  points2[i+2] = get_transformed_point_2d(x + unif(rng), 0.5 + unif(rng));
  points2[i+3] = get_transformed_point_2d(x + unif(rng), 1.5 + unif(rng));
  points2[i+4] = get_transformed_point_2d(x + unif(rng), 2.5 + unif(rng));
  points2[i+5] = get_transformed_point_2d(x + unif(rng), 4 + unif(rng));
  points2[i+6] = get_transformed_point_2d(x + unif(rng), 5 + unif(rng));
  i = 7; x = 0;
  points2[i+0] = get_transformed_point_2d(x + unif(rng), 0.2 + unif(rng));
  points2[i+1] = get_transformed_point_2d(x + unif(rng), 0 + unif(rng));
  points2[i+2] = get_transformed_point_2d(x + unif(rng), 0.5 + unif(rng));
  points2[i+3] = get_transformed_point_2d(x + unif(rng), 1.5 + unif(rng));
  points2[i+4] = get_transformed_point_2d(x + unif(rng), 2.5 + unif(rng));
  points2[i+5] = get_transformed_point_2d(x + unif(rng), 4 + unif(rng));
  points2[i+6] = get_transformed_point_2d(x + unif(rng), 5 + unif(rng));
  i = 14; x = 1;
  points2[i+0] = get_transformed_point_2d(x + unif(rng), 0.15 + unif(rng));
  points2[i+1] = get_transformed_point_2d(x + unif(rng), 0 + unif(rng));
  points2[i+2] = get_transformed_point_2d(x + unif(rng), 0.5 + unif(rng));
  points2[i+3] = get_transformed_point_2d(x + unif(rng), 1.5 + unif(rng));
  points2[i+4] = get_transformed_point_2d(x + unif(rng), 2.5 + unif(rng));
  points2[i+5] = get_transformed_point_2d(x + unif(rng), 4 + unif(rng));
  points2[i+6] = get_transformed_point_2d(x + unif(rng), 5 + unif(rng));

  std::function<double(int, int)> diss_function = [=](int j, int i) {

    typedef std::chrono::high_resolution_clock myclock;
    unsigned seed = std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::system_clock::now().time_since_epoch()).count();

    std::mt19937_64 rng(seed);
    std::uniform_real_distribution<double> unif(-0.05,0.05);

    std::vector< double> feature_i =
    {0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4,
     0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
     0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
     0.3, 0.3, 0.3, 0.3, 0.3, 0.3, 0.3,
     0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4};

    std::vector< double> feature_j =
    {0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4,
     0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26,
     0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2,
     0.26, 0.26, 0.26, 0.26, 0.26, 0.26, 0.26,
     0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4};

    int trajectory_offset = 7;
    std::mt19937_64 rng_i(i);
    std::uniform_real_distribution<double> unif_i(-0.05,0.05);  
    double featurei = feature_i[i] + unif_i(rng_i);;

    std::mt19937_64 rng_j(j);
    std::uniform_real_distribution<double> unif_j(-0.05,0.05);  
    //  double featurej = feature_j[j+trajectory_offset] + unif_j(rng_j);;
    double featurej = feature_i[j+trajectory_offset] + unif_j(rng_j);;  
    
    return pow(featurei - featurej, 2.0);
  };

  ConvexTransformationEstimation::GraphAlignmentProblem2D problem(
      points2,
      points,
      diss_function);

  ConvexTransformationEstimation::SolverRigidBody solver(3, false);
  auto result = solver.solve_problem(problem);

  double PI = 3.141592653589793238462643383279;
  ASSERT_LE(acos(result.get_R()[0]) * 180/PI, 33);
  ASSERT_GE(acos(result.get_R()[0]) * 180/PI, 27);
  ASSERT_LE(result.get_t()[0], 23);
  ASSERT_GE(result.get_t()[0], 17);
  ASSERT_LE(result.get_t()[1], -7);
  ASSERT_GE(result.get_t()[1], -13);    
}

TEST(Test2DProblemClass, RigidBodyTest2) {

  ConvexTransformationEstimation::SolverRigidBody solver(6, false);
  auto result = Solve2DProblemFromFile( std::string(TEST_DATA_PATH) + "/0_lawn_a.csv" ,
                                                  std::string(TEST_DATA_PATH) + "/0_lawn_b.csv",
                                                  std::string(TEST_DATA_PATH) + "/0_params.csv",
                                                  solver);

  double PI = 3.141592653589793238462643383279;
  ASSERT_LE(acos(result.get_R()[0]) * 180/PI, 32);
  ASSERT_GE(acos(result.get_R()[0]) * 180/PI, 21);
  ASSERT_LE(result.get_t()[0], 28);
  ASSERT_GE(result.get_t()[0], 24);
  ASSERT_LE(result.get_t()[1], -3);
  ASSERT_GE(result.get_t()[1], -7);    
}
