# README #

This repository implements the method proposed in the following paper:

J. G. Mangelson, R. Vasudevan, and R. M. Eustice, “Communication Constrained Trajectory Alignment For
Multi-Agent Inspection via Linear Programming," in Proceedings of the IEEE/MTS OCEANS Conference and
Exhibition, Charleston, SC, October 2018.

### Convex Transformation Estimation (CTE) ###

Convex Transformation Estimation is a library that takes a query set of feature points and a reference set of 
feature points and tries to find a transformation that aligns the query points with the reference feature points
such that feature values are consistent. By using convex relaxation/appoximation techniques, the method does
not require an initialization and can take advantage of existing convex optimization libraries such as MOSEK.
The library is able to estimate transformations of the following types: affine, symmetric, rigid-body (isometric).
The current implementation handles two dimensional transformations (Ex: SE(2) or R^2x2) as well as the special case
where the transformation is in three dimensions but the third dimension (Ex: Depth) is well known.

The algorithms used for affine and symmetric transformations are based on:

H. Li, X. Huang, J. Huang, and S. Zhang, "Feature matching with affine-function transformation models," 
IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 36, no. 12, pp. 2407-2422, 2014.

If you use this code in a publication or for research, please cite both of the above papers. 

### How to build the code? ###

*** Required third party libs: ***

* [Eigen3](http://eigen.tuxfamily.org/index.php?title=Main_Page)

* [qhull](https://launchpad.net/ubuntu/+source/qhull)

* [pthread](https://packages.debian.org/sid/libpthread-stubs0-dev)

* [MOSEK 8 Fusion C++](https://docs.mosek.com/8.1/install/installation.html)

This software is constructed according to the [Pods software policies and templates](http://sourceforge.net/projects/pods).

*** To install: ***

**Local:** `make`. The project will search 4 levels up the tree for
a `build/` directory. If one is not found it will be installed to
`build/` in the local directory.

**System-wide:** `make BUILD_PREFIX=<build_location>` as root.

### How to use the code? ###

An example is included in `src/cte_tests/example_tests.cpp`

### Testing ###
This project uses [GTest](https://github.com/google/googletest) for unit testing and will automatically download it locally when
you build.

Individual test binaries can be found in `test/` in the build directory.
`make test` will run all tests.

### License and Attribution ###

You can freely use this software as long as you cite our work and follow the license and copywrite notice under 
LICENSE. 

### Questions or Contact ###

If you use this code in your research, we would love hear about it. 
In you have any questions, you can contact us via the following email:

Joshua Mangelson
mangelso@umich.edu
