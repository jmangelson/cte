#
# Try to find MOSEK
# Once done this will define
#
# MOSEK_FOUND           - system has MOSEK
# MOSEK_INCLUDE_DIRS    - the MOSEK include directories
# MOSEK_LIB             - Link this to use MOSEK
# MOSEK_FUSION_LIB      - Link these to use FUSION
#

# Hardcoded search paths
set(SEARCH_PATHS
  /usr/local/mosek/7/tools/platform/osx64x86/
  /usr/local/mosek/8/tools/platform/osx64x86/
  /opt/mosek/7/tools/platform/linux64x86/
  /opt/mosek/8/tools/platform/linux64x86/
)

find_path(MOSEK_INCLUDE_DIR mosek.h
  PATHS ${SEARCH_PATHS}
  PATH_SUFFIXES h
)

set(MOSEK_LIB)
find_library(MOSEK_LIB NAMES mosek64
  HINT
    "${MOSEK_INCLUDE_DIR}"
    "${MOSEK_INCLUDE_DIR}/../bin"
    "${MOSEK_INCLUDE_DIR}/lib"
  PATHS
    ${SEARCH_PATHS}
  NO_DEFAULT_PATH
  PATH_SUFFIXES so a bin lib dylib)

set(MOSEK_FUSION_LIB)
find_library(MOSEK_FUSION_LIB NAMES fusion64
  HINT
    "${MOSEK_INCLUDE_DIR}"
    "${MOSEK_INCLUDE_DIR}/../bin"
    "${MOSEK_INCLUDE_DIR}/lib"
  PATHS
    ${SEARCH_PATHS}
  NO_DEFAULT_PATH
  PATH_SUFFIXES so a bin lib dylib)

# Check that Mosek was successfully found
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(
  MOSEK DEFAULT_MSG MOSEK_LIB MOSEK_INCLUDE_DIR
)
set(MOSEK_INCLUDE_DIRS ${MOSEK_INCLUDE_DIR})

# Hide variables from CMake-Gui options
mark_as_advanced(MOSEK_LIB MOSEK_FUSION_LIB MOSEK_INCLUDE_DIRS MOSEK_INCLUDE_DIR)
